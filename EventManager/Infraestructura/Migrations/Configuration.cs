namespace Infraestructura.Migrations
{
    using Dominio.Entidades;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Infraestructura.EventManagerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Infraestructura.EventManagerContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var listaBancos = new List<ListaBancos>
            {
                new ListaBancos { Nombre = "Nacion"},
                new ListaBancos { Nombre = "Macro"},
                new ListaBancos { Nombre = "BBVA Frances"},
                new ListaBancos { Nombre = "Santander Rio"},
                new ListaBancos { Nombre = "Galicia"},
                new ListaBancos { Nombre = "Santiago Del Estero"},
                new ListaBancos { Nombre = "HSBC"},
                new ListaBancos { Nombre = "Comafi"}
            };

            foreach (ListaBancos e in listaBancos)
            {
                var listaBancosDataBase = context.ListaBancos.Where(
                    x => x.Nombre == e.Nombre);

                if (!listaBancosDataBase.Any())
                {
                    context.ListaBancos.AddOrUpdate(e);
                }

                context.SaveChanges();
            }

            var listaTarjetas = new List<TipoTarjeta>
            {
                new TipoTarjeta { Nombre = "Visa Credito"},
                new TipoTarjeta { Nombre = "Visa Debito"},
                new TipoTarjeta { Nombre = "Master Card Credito"},
                new TipoTarjeta { Nombre = "Master Card Debito"},
                new TipoTarjeta { Nombre = "Maestro Credito"},
            };

            foreach (TipoTarjeta e in listaTarjetas)
            {
                var listaTarjetasDataBase = context.TipoTarjeta.Where(
                    x => x.Nombre == e.Nombre);

                if (!listaTarjetasDataBase.Any())
                {
                    context.TipoTarjeta.AddOrUpdate(e);
                }

                context.SaveChanges();
            }

            var listaCategorias = new List<Categoria_Evento>
            {
                new Categoria_Evento { Descripcion = "Informatica", Estado = true},
                new Categoria_Evento { Descripcion = "Deportes", Estado = true},
                new Categoria_Evento { Descripcion = "Gastronomia", Estado = true},
                new Categoria_Evento { Descripcion = "Arte", Estado = true},
                new Categoria_Evento { Descripcion = "Cultura", Estado = true},
                new Categoria_Evento { Descripcion = "Astronomia", Estado = true}
            };

            foreach (Categoria_Evento e in listaCategorias)
            {
                var listaCategoriasDataBase = context.Categoria.Where(
                    x => x.Estado == e.Estado);

                if (!listaCategoriasDataBase.Any())
                {
                    context.Categoria.AddOrUpdate(e);
                }

                context.SaveChanges();
            }

            var listaProvincias = new List<Provincia>
            {
                new Provincia { Nombre = "Buenos Aires"},
                new Provincia { Nombre = "Catamarca	"},
                new Provincia { Nombre = "Chaco"},
                new Provincia { Nombre = "Chubut"},
                new Provincia { Nombre = "C�rdoba"},
                new Provincia { Nombre = "Corrientes"},
                new Provincia { Nombre = "Entre R�os"},
                new Provincia { Nombre = "Formosa"},
                new Provincia { Nombre = "Jujuy"},
                new Provincia { Nombre = "La Pampa"},
                new Provincia { Nombre = "La Rioja"},
                new Provincia { Nombre = "Mendoza"},
                new Provincia { Nombre = "Misiones"},
                new Provincia { Nombre = "Neuqu�n"},
                new Provincia { Nombre = "R�o Negro"},
                new Provincia { Nombre = "Salta"},
                new Provincia { Nombre = "San Juan"},
                new Provincia { Nombre = "San Luis"},
                new Provincia { Nombre = "Santa Cruz"},
                new Provincia { Nombre = "Santa Fe"},
                new Provincia { Nombre = "Santiago del Estero"},
                new Provincia { Nombre = "Tierra del Fuego"},
                new Provincia { Nombre = "Tucum�n"}

            };

            foreach (Provincia e in listaProvincias)
            {
                var listaProvinciasDataBase = context.Provincia.Where(
                    x => x.Nombre == e.Nombre);

                if (!listaProvinciasDataBase.Any())
                {
                    context.Provincia.AddOrUpdate(e);
                }

                context.SaveChanges();
            }

            var listaCiudades = new List<Ciudad>
            {
                new Ciudad { Nombre = "Adolfo Alsina", CP = 6430, ProvinciaId = 1 },
                new Ciudad { Nombre = "Avellaneda", CP = 1870, ProvinciaId = 1 },
                new Ciudad { Nombre = "San Fernando del Valle de Catamarca", CP = 4751, ProvinciaId = 2 },
                new Ciudad { Nombre = "Santa Maria", CP = 4139, ProvinciaId = 2 },
                new Ciudad { Nombre = "Resistencia", CP = 3500, ProvinciaId = 3 },
                new Ciudad { Nombre = "Barranqueras", CP = 3503, ProvinciaId = 3 },
                new Ciudad { Nombre = "Trelew", CP = 9100, ProvinciaId = 4 },
                new Ciudad { Nombre = "Puerto Madryn", CP = 9120, ProvinciaId = 4 },
                new Ciudad { Nombre = "Carlos Paz", CP = 5152, ProvinciaId = 5 },
                new Ciudad { Nombre = "Jesus Maria", CP = 5223, ProvinciaId = 5 },
                new Ciudad { Nombre = "Paso de la Patria", CP = 3409, ProvinciaId = 6 },
                new Ciudad { Nombre = "Ituzaingo", CP = 3302, ProvinciaId = 6 },
                new Ciudad { Nombre = "Parana", CP = 3100, ProvinciaId = 7 },
                new Ciudad { Nombre = "Concordia", CP = 3200, ProvinciaId = 7 },
                new Ciudad { Nombre = "Las Lomitas", CP = 3630, ProvinciaId = 8 },
                new Ciudad { Nombre = "Clorinda", CP = 3610, ProvinciaId = 8 },
                new Ciudad { Nombre = "San Salvador de Jujuy", CP = 4600, ProvinciaId = 9 },
                new Ciudad { Nombre = "San Pedro", CP = 4500, ProvinciaId = 9 },
                new Ciudad { Nombre = "Santa Rosa", CP = 6300, ProvinciaId = 10 },
                new Ciudad { Nombre = "General Pico", CP = 6360, ProvinciaId = 10 },
                new Ciudad { Nombre = "Chalecito", CP = 5360, ProvinciaId = 11 },
                new Ciudad { Nombre = "Alfaro", CP = 26540, ProvinciaId = 11 },
                new Ciudad { Nombre = "Godoy Cruz", CP = 5501, ProvinciaId = 12 },
                new Ciudad { Nombre = "Lujan de Cuyo", CP = 5507, ProvinciaId = 12 },
                new Ciudad { Nombre = "Posadas", CP = 3300, ProvinciaId = 13 },
                new Ciudad { Nombre = "Puerto Iguazu", CP = 3370, ProvinciaId = 13 },
                new Ciudad { Nombre = "Neuquen", CP = 8300, ProvinciaId = 14 },
                new Ciudad { Nombre = "Plottier", CP = 8316, ProvinciaId = 14 },
                new Ciudad { Nombre = "General Roca", CP = 8332, ProvinciaId = 15 },
                new Ciudad { Nombre = "Cipolletti", CP = 8324, ProvinciaId = 15 },
                new Ciudad { Nombre = "Metan", CP = 4440, ProvinciaId = 16 },
                new Ciudad { Nombre = "Guemes", CP = 4430, ProvinciaId = 16 },
                new Ciudad { Nombre = "Rivadavia", CP = 5400, ProvinciaId = 17 },
                new Ciudad { Nombre = "Santa Lucia", CP = 5411, ProvinciaId = 17 },
                new Ciudad { Nombre = "Juan Mart�n de Pueyrred�n", CP = 5700, ProvinciaId = 18 },
                new Ciudad { Nombre = "San Francisco del Monte de Oro", CP = 5705, ProvinciaId = 18 },
                new Ciudad { Nombre = "R�o Gallegos", CP = 9400, ProvinciaId = 19 },
                new Ciudad { Nombre = "El Calafate", CP = 9405, ProvinciaId = 19 },
                new Ciudad { Nombre = "Rosario", CP = 2000, ProvinciaId = 20 },
                new Ciudad { Nombre = "Reconquista", CP = 3560, ProvinciaId = 20 },
                new Ciudad { Nombre = "Santiago del Estero", CP = 4200, ProvinciaId = 21 },
                new Ciudad { Nombre = "La Banda", CP = 4202, ProvinciaId = 21 },
                new Ciudad { Nombre = "Ushuaia ", CP = 9410, ProvinciaId = 22 },
                new Ciudad { Nombre = "Rio Grande", CP = 9420, ProvinciaId = 22 },
                new Ciudad { Nombre = "San Miguel de Tucuman", CP = 4000, ProvinciaId = 23 },
                new Ciudad { Nombre = "Yerba Buena", CP = 4001, ProvinciaId = 23 }


            };

            foreach (Ciudad e in listaCiudades)
            {
                var listaCiudadesDataBase = context.Ciudad.Where(
                    x => x.Nombre == e.Nombre);

                if (!listaCiudadesDataBase.Any())
                {
                    context.Ciudad.AddOrUpdate(e);
                }

                context.SaveChanges();
            }

        }
    }
}
