﻿using Dominio.Repositorio.Organizador;
using Repositorio;

namespace Infraestructura.Repositorio.Organizador
{
    public class OrganizadorRepositorio : Repositorio<Dominio.Entidades.Organizador>, IOrganizadorRepositorio
    {

    }
}
