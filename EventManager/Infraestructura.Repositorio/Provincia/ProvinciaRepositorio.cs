﻿using Dominio.Repositorio.Provincia;
using Repositorio;

namespace Infraestructura.Repositorio.Provincia
{
    public class ProvinciaRepositorio : Repositorio<Dominio.Entidades.Provincia>, IProvinciaRepositorio
    {

    }
}
