﻿using Dominio.Repositorio.Persona;
using Repositorio;

namespace Infraestructura.Repositorio.Persona
{
    public class PersonaRepositorio : Repositorio<Dominio.Entidades.Persona>, IPersonaRepositorio
    {

    }
}
