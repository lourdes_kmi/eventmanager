﻿using Dominio.Entidades;
using Dominio.Repositorio.Evento;
using Infraestructura;
using IServicio.Entrada;
using IServicio.Evento;
using IServicio.Evento.Dto;
using Servicio.Base;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Evento
{
    public class EventoServicio : ServicioBase, IEventoServicio
    {
        private EventManagerContext db = new EventManagerContext();

        private readonly IEventoRepositorio _eventoRepositorio;
        private readonly IEntradaServicio _entradaServicio;

        public EventoServicio(IEventoRepositorio eventoRepositorio, IEntradaServicio entradaServicio)
        {
            _eventoRepositorio = eventoRepositorio;
            _entradaServicio = entradaServicio;
        }

        //Servicios

        public void Add(EventoDto Dto)
        {
            var Evento = new Dominio.Entidades.Evento()
            {
                Nombre = Dto.Nombre,
                Descripcion = Dto.Descripcion,
                Imagen = Dto.Imagen,
                FechaInicio = Dto.FechaInicio,
                FechaFin = Dto.FechaFin,
                HoraInicio = Dto.HoraInicio,
                HoraFin = Dto.HoraFin,
                Lugar = Dto.Lugar,
                Calle = Dto.Calle,
                Numero = Dto.Numero,
                Referencias = Dto.Referencias,
                OrganizadorId = Dto.OrganizadorId,
                OradorId = Dto.OradorId,
                ProvinciaId = Dto.ProvinciaId,
                CiudadId = Dto.CiudadId,
                Categoria_EventoId = Dto.Categoria_EventoId,
                UsuarioNombre = Dto.UsuarioNombre,
                RowVersion = Dto.RowVersion

            };

            _eventoRepositorio.Agregar(Evento);
            _eventoRepositorio.Save();
        }

        public void Delete(long Id)
        {
            _eventoRepositorio.Eliminar(Id);
            _eventoRepositorio.Save();
        }

        public IEnumerable<EventoDto> GetByFilter(string cadenaBuscar)
        {
            return _eventoRepositorio.ObtenerPorFlitro(x => x.Nombre.Contains(cadenaBuscar), "Categoria_Evento, Organizador, Orador, Provincia, Ciudad")
            .Select(x => new EventoDto()
            {
                Id = x.Id,
                Categoria_EventoId = x.Categoria_EventoId,
                CategoriaStr = x.Categoria_Evento.Descripcion,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                Imagen = x.Imagen,
                FechaInicio = x.FechaInicio,
                FechaFin = x.FechaFin,
                HoraInicio = x.HoraInicio,
                HoraFin = x.HoraFin,
                Lugar = x.Lugar,
                Calle = x.Calle,
                Numero = x.Numero,
                Referencias = x.Referencias,
                OrganizadorId = x.OrganizadorId,
                OrganizadorStr = x.Organizador.Nombre + " " + x.Organizador.Apellido,
                OradorId = x.OradorId,
                OradorStr = x.Orador.Nombre + " " + x.Orador.Apellido,
                ProvinciaId = x.ProvinciaId,
                ProvinciaStr = x.Provincia.Nombre,
                CiudadId = x.CiudadId,
                CiudadStr = x.Ciudad.Nombre
            });
        }

        public EventoDto GetById(long? Id)
        {
            var Evento = _eventoRepositorio.ObtenerPorId(Id, "Categoria_Evento, Organizador, Orador, Provincia, Ciudad");

            return new EventoDto()
            {
                Id = Evento.Id,
                Categoria_EventoId = Evento.Categoria_EventoId,
                CategoriaStr = Evento.Categoria_Evento.Descripcion,
                Nombre = Evento.Nombre,
                Descripcion = Evento.Descripcion,
                Imagen = Evento.Imagen,
                FechaInicio = Evento.FechaInicio.Date,
                FechaFin = Evento.FechaFin.Date,
                HoraInicio = Evento.HoraInicio,
                HoraFin = Evento.HoraFin,
                Lugar = Evento.Lugar,
                Calle = Evento.Calle,
                Numero = Evento.Numero,
                Referencias = Evento.Referencias,
                OrganizadorId = Evento.OrganizadorId,
                OrganizadorStr = Evento.Organizador.Nombre + " " + Evento.Organizador.Apellido,
                OradorId = Evento.OradorId,
                OradorStr = Evento.Orador.Nombre + " " + Evento.Orador.Apellido,
                ProvinciaId = Evento.ProvinciaId,
                ProvinciaStr = Evento.Provincia.Nombre,
                CiudadId = Evento.CiudadId,
                CiudadStr = Evento.Ciudad.Nombre
            };
        }

        public IEnumerable<EventoDto> GetByUserName(string usuarioNombre)
        {

            var evento = _eventoRepositorio.ObtenerTodo("Categoria_Evento, Organizador, Orador, Provincia, Ciudad").Where(x => x.UsuarioNombre == usuarioNombre);

            return evento
            .Select(x => new EventoDto()
            {
                Id = x.Id,
                Categoria_EventoId = x.Categoria_EventoId,
                CategoriaStr = x.Categoria_Evento.Descripcion,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                Imagen = x.Imagen,
                FechaInicio = x.FechaInicio,
                FechaFin = x.FechaFin,
                HoraInicio = x.HoraInicio,
                HoraFin = x.HoraFin,
                Lugar = x.Lugar,
                Calle = x.Calle,
                Numero = x.Numero,
                Referencias = x.Referencias,
                OrganizadorId = x.OrganizadorId,
                OrganizadorStr = x.Organizador.Nombre + " " + x.Organizador.Apellido,
                OradorId = x.OradorId,
                OradorStr = x.Orador.Nombre + " " + x.Orador.Apellido,
                ProvinciaId = x.ProvinciaId,
                ProvinciaStr = x.Provincia.Nombre,
                CiudadId = x.CiudadId,
                CiudadStr = x.Ciudad.Nombre,
                UsuarioNombre = x.UsuarioNombre
            });
        }

        public void Update(EventoDto Dto)
        {
            var Evento = _eventoRepositorio.ObtenerPorId(Dto.Id, "Categoria_Evento, Organizador, Orador, Provincia, Ciudad");

            Evento.Categoria_EventoId = Dto.Categoria_EventoId;
            Evento.Nombre = Dto.Nombre;
            Evento.Descripcion = Dto.Descripcion;
            Evento.Imagen = Dto.Imagen;
            Evento.FechaInicio = Dto.FechaInicio;
            Evento.FechaFin = Dto.FechaFin;
            Evento.HoraInicio = Dto.HoraInicio;
            Evento.HoraFin = Dto.HoraFin;
            Evento.Lugar = Dto.Lugar;
            Evento.Calle = Dto.Calle;
            Evento.Numero = Dto.Numero;
            Evento.Referencias = Dto.Referencias;
            Evento.OrganizadorId = Dto.OrganizadorId;
            Evento.OradorId = Dto.OradorId;
            Evento.ProvinciaId = Dto.ProvinciaId;
            Evento.CiudadId = Dto.CiudadId;

            _eventoRepositorio.Modificar(Evento);
            _eventoRepositorio.Save();

        }

        public void AsignarOrador(long? enventoId, long oradorId)
        {
            var Evento = _eventoRepositorio.ObtenerPorId(enventoId, "Orador");

            Evento.OradorId = oradorId;

            _eventoRepositorio.Modificar(Evento);
            _eventoRepositorio.Save();
        }

        public void AsignarOrganizador(long? eventoId, long organizadorId)
        {
            var Evento = _eventoRepositorio.ObtenerPorId(eventoId, "Organizador");

            Evento.OrganizadorId = organizadorId;

            _eventoRepositorio.Modificar(Evento);
            _eventoRepositorio.Save();
        }

        public IEnumerable<EventoDto> GetAll()
        {
           return _eventoRepositorio.ObtenerTodo("Categoria_Evento, Organizador, Orador, Provincia, Ciudad")
                 .Select(x => new EventoDto()
                 {
                     Id = x.Id,
                     Categoria_EventoId = x.Categoria_EventoId,
                     CategoriaStr = x.Categoria_Evento.Descripcion,
                     Nombre = x.Nombre,
                     Descripcion = x.Descripcion,
                     Imagen = x.Imagen,
                     FechaInicio = x.FechaInicio,
                     FechaFin = x.FechaFin,
                     HoraInicio = x.HoraInicio,
                     HoraFin = x.HoraFin,
                     Lugar = x.Lugar,
                     Calle = x.Calle,
                     Numero = x.Numero,
                     Referencias = x.Referencias,
                     OrganizadorId = x.OrganizadorId,
                     OrganizadorStr = x.Organizador.Nombre + " " + x.Organizador.Apellido,
                     OradorId = x.OradorId,
                     OradorStr = x.Orador.Nombre + " " + x.Orador.Apellido,
                     ProvinciaId = x.ProvinciaId,
                     ProvinciaStr = x.Provincia.Nombre,
                     CiudadId = x.CiudadId,
                     CiudadStr = x.Ciudad.Nombre
                 });
        }
    }
}
