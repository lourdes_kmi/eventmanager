﻿using Dominio.Repositorio.Resumen;
using IServicio.Resumen;
using IServicio.Resumen.Dto;
using Servicio.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Resumen
{
    public class ResumenServicio : ServicioBase, IResumenServicio
    {
        private readonly IResumenRepositorio _resumenRepositorio;

        public ResumenServicio(IResumenRepositorio resumenRepositorio)
        {
            _resumenRepositorio = resumenRepositorio;
        }

        public void Add(ResumenDto Dto)
        {
            var resumen = new Dominio.Entidades.Resumen
            {
                Total = Dto.Total
            };

            _resumenRepositorio.Agregar(resumen);
            _resumenRepositorio.Save();
        }

        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ResumenDto> GetByFilter(string cadenaBuscar)
        {
            throw new NotImplementedException();
        }

        public ResumenDto GetById(long? Id)
        {
            throw new NotImplementedException();
        }

        public ResumenDto ObtenerUltimoResumen()
        {
            var lista = _resumenRepositorio.ObtenerTodo();

            var id = lista.Any() ? lista.Max(x => x.Id) : 1;

            var resumen = _resumenRepositorio.ObtenerPorId(id);

            return new ResumenDto()
            {
                Id = resumen.Id,
                Total = resumen.Total
            };
        }

        public void Update(ResumenDto Dto)
        {
            throw new NotImplementedException();
        }
    }
}
