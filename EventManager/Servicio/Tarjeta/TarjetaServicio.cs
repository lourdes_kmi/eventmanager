﻿using Dominio.Repositorio.Tarjeta;
using IServicio.Tarjeta;
using IServicio.Tarjeta.Dto;
using Servicio.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Tarjeta
{
    public class TarjetaServicio : ServicioBase, ITarjetaServicio
    {
        private readonly ITarjetaRepositorio _tarjetaRepositorio;

        public TarjetaServicio(ITarjetaRepositorio tarjetaRepositorio)
        {
            _tarjetaRepositorio = tarjetaRepositorio;
        }

        public void Add(TarjetaDto Dto)
        {
            var tarjeta = new Dominio.Entidades.Tarjeta
            {
                Id = ObtenerSiguienteId(),
                NombreTitular = Dto.NombreTitular,
                BancoEmisor = Dto.BancoEmisor,
                CodigoSeguridad = Dto.CodigoSeguridad,
                Cuotas = Dto.Cuotas,
                DniTitular = Dto.DniTitular,
                FechaExpiracion = Dto.FechaExpiracion,  
                Numero = Dto.Numero,
                TipoTarjeta = Dto.TipoTarjeta

            };

            _tarjetaRepositorio.Agregar(tarjeta);
            _tarjetaRepositorio.Save();
        }

        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TarjetaDto> GetByFilter(string cadenaBuscar)
        {
            return _tarjetaRepositorio.ObtenerPorFlitro(x => x.NombreTitular.Contains(cadenaBuscar))
                .Select(x => new TarjetaDto()
                {
                    Id = x.Id,
                    NombreTitular = x.NombreTitular,
                    BancoEmisor = x.BancoEmisor,
                    CodigoSeguridad = x.CodigoSeguridad,
                    DniTitular = x.DniTitular,
                    FechaExpiracion = x.FechaExpiracion,
                    Numero = x.Numero,
                    TipoTarjeta = x.TipoTarjeta,
                    Cuotas = x.Cuotas

                }).ToList();
        }

        public TarjetaDto GetById(long? Id)
        {
            throw new NotImplementedException();
        }

        public void Update(TarjetaDto Dto)
        {
            throw new NotImplementedException();
        }

        public long ObtenerSiguienteId()
        {

            var lista = _tarjetaRepositorio.ObtenerTodo();

            return lista.Any() ? lista.Max(x => x.Id) : 1;

        }
          

    }
}
