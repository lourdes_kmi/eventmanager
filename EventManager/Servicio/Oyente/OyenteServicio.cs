﻿using Dominio.Repositorio.Oyente;
using IServicio.Oyente;
using IServicio.Oyente.Dto;
using Servicio.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Oyente
{
    public class OyenteServicio : ServicioBase, IOyenteServicio
    {
        private readonly IOyenteRepositorio _oyenteRepositorio;

        public OyenteServicio(IOyenteRepositorio oyenteRepositorio)
        {
            _oyenteRepositorio = oyenteRepositorio;
        }

        public void Add(OyenteDto Dto)
        {
            var oyente = new Dominio.Entidades.Oyente()
            {
                Id = ObtenerSiguienteId(),
                Apellido = Dto.Apellido,
                Nombre = Dto.Nombre,
                Celular = Dto.Celular,
                Direccion = Dto.Direccion,
                EventoId = Dto.EventoId,
                EntradaId = Dto.EntradaId,
                Estado = Dto.Estado
            };

            _oyenteRepositorio.Agregar(oyente);
            _oyenteRepositorio.Save();
        }

        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OyenteDto> GetByFilter(string cadenaBuscar)
        {
            return _oyenteRepositorio.ObtenerPorFlitro(x => x.Apellido.Contains(cadenaBuscar))
                 .Select(x => new OyenteDto()
                 {
                     Id = x.Id,
                     Apellido = x.Apellido,
                     Nombre = x.Nombre,
                     Celular = x.Celular,
                     Direccion = x.Direccion,
                     Estado = x.Estado,
                     CodigoInscripcion = x.CodigoInscripcion,
                     NumeroTransaccion = x.NumeroTransaccion,
                     EventoId = x.EventoId,
                     EntradaId = x.EntradaId


                 }).ToList();
        }

        public IEnumerable<OyenteDto> GetByEventoId(long id)
        {
            return _oyenteRepositorio.ObtenerPorFlitro(x => x.EventoId == id)
                 .Select(x => new OyenteDto()
                 {
                     Id = x.Id,
                     Apellido = x.Apellido,
                     Nombre = x.Nombre,
                     Celular = x.Celular,
                     Direccion = x.Direccion,
                     Estado = x.Estado,
                     CodigoInscripcion = x.CodigoInscripcion,
                     NumeroTransaccion = x.NumeroTransaccion,
                     EventoId = x.EventoId,
                     EntradaId = x.EntradaId


                 }).ToList();
        }

        public OyenteDto GetById(long? Id)
        {
            var oyente = _oyenteRepositorio.ObtenerPorId(Id);

            return new OyenteDto()
            {
                Id = oyente.Id,
                Nombre = oyente.Nombre,
                Apellido = oyente.Apellido,
                Celular = oyente.Celular,
                CodigoInscripcion = oyente.CodigoInscripcion,
                Direccion = oyente.Direccion,
                NumeroTransaccion = oyente.NumeroTransaccion,
                EntradaId = oyente.EntradaId,
                Estado = oyente.Estado
            };
        }

        public OyenteDto ObtenerPorFiltroAvanzado(string nombre, string apellido, string Celular, string direccion, long eventoId, long entradaId)
        {
            var oyente = _oyenteRepositorio.ObtenerTodo().FirstOrDefault(x => x.Nombre == nombre && x.Apellido == apellido && x.Celular == Celular && x.Direccion == direccion && x.EventoId == eventoId && x.EntradaId == entradaId);


            return new OyenteDto()
            {
                Id = oyente.Id,
                Nombre = oyente.Nombre,
                Apellido = oyente.Apellido,
                Celular = oyente.Celular,
                Direccion = oyente.Direccion,
                Estado = oyente.Estado,
                EventoId = oyente.EventoId,
                EntradaId = oyente.EntradaId,
                CodigoInscripcion = oyente.CodigoInscripcion,
                NumeroTransaccion = oyente.NumeroTransaccion
            };

        }

        public long ObtenerSiguienteId()
        {
            var lista = _oyenteRepositorio.ObtenerTodo();

            return lista.Any() ? lista.Max(x => x.Id) : 1;
        }

        public void Update(OyenteDto Dto)
        {
            var oyente = _oyenteRepositorio.ObtenerPorId(Dto.Id);

            oyente.Nombre = Dto.Nombre;
            oyente.Apellido = Dto.Apellido;
            oyente.Celular = Dto.Celular;
            oyente.CodigoInscripcion = Dto.CodigoInscripcion;
            oyente.NumeroTransaccion = Dto.NumeroTransaccion;
            oyente.Direccion = Dto.Direccion;
            oyente.Estado = Dto.Estado;

            _oyenteRepositorio.Modificar(oyente);
            _oyenteRepositorio.Save();
        }

        public IEnumerable<OyenteDto> ObtenerOyentesUltimaVenta(int numeroTransaccion)
        {
            var lista = _oyenteRepositorio.ObtenerTodo().Where(x => x.NumeroTransaccion == numeroTransaccion);

                 return lista
                 .Select(x => new OyenteDto()
                 {
                     Id = x.Id,
                     Apellido = x.Apellido,
                     Nombre = x.Nombre,
                     Celular = x.Celular,
                     Direccion = x.Direccion,
                     Estado = x.Estado,
                     EventoId = x.EventoId,
                     EntradaId = x.EntradaId,
                     CodigoInscripcion = x.CodigoInscripcion,
                     NumeroTransaccion = x.NumeroTransaccion


                 }).ToList();


        }
    }
}
