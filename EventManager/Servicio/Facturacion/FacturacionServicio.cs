﻿using Dominio.Repositorio.Facturacion;
using IServicio.Facturacion;
using IServicio.Faturacion.Dto;
using Servicio.Base;
using System;
using System.Collections.Generic;

namespace Servicio.Facturacion
{
    public class FacturacionServicio : ServicioBase, IFacturacionServicio
    {
        private readonly IFacturacionRepositorio _facturacionRepositorio;

        public FacturacionServicio(IFacturacionRepositorio facturacionRepositorio)
        {
            _facturacionRepositorio = facturacionRepositorio;
        }

        public void Add(FacturacionDto Dto)
        {
            var Facturacion = new Dominio.Entidades.Facturacion()
            {
                Direccion = Dto.Direccion,
                CodPostal = Dto.CodPostal,
                CiudadId = Dto.CiudadId,
                ProvinciaId = Dto.ProvinciaId,
                TarjetaId = Dto.TarjetaId
            };

            _facturacionRepositorio.Agregar(Facturacion);
            _facturacionRepositorio.Save();
        }

        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FacturacionDto> GetByFilter(string cadenaBuscar)
        {
            throw new NotImplementedException();
        }

        public FacturacionDto GetById(long? Id)
        {
            throw new NotImplementedException();
        }

        public void Update(FacturacionDto Dto)
        {
            throw new NotImplementedException();
        }
    }
}
