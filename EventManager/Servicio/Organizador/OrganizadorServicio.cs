﻿using Dominio.Repositorio.Organizador;
using IServicio.Organizador;
using IServicio.Organizador.Dto;
using Servicio.Base;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Organizador
{
    public class OrganizadorServicio : ServicioBase, IOrganizadorServicio
    {
        private readonly IOrganizadorRepositorio _organizadorRepositorio;

        public OrganizadorServicio(IOrganizadorRepositorio organizadorRepositorio)
        {
            _organizadorRepositorio = organizadorRepositorio;
        }

        public void Add(OrganizadorDto Dto)
        {
            var Organizador = new Dominio.Entidades.Organizador()
            {
                Legajo = ObtenerSiguienteLegajo(),
                Apellido = Dto.Apellido,
                Nombre = Dto.Nombre,
                Celular = Dto.Celular,
                Cuil = Dto.Cuil,
                FechaNacimiento = Dto.FechaNacimiento,
                Genero = Dto.Genero,
                Telefono = Dto.Telefono,
                Estado = true,
                CiudadId = Dto.CiudadId,
                ProvinciaId = Dto.ProvinciaId,
                RowVersion = Dto.RowVersion

            };

            _organizadorRepositorio.Agregar(Organizador);
            _organizadorRepositorio.Save();

        }

        public void Delete(long Id)
        {
            _organizadorRepositorio.Eliminar(Id);
            _organizadorRepositorio.Save();
        }

        public IEnumerable<OrganizadorDto> GetByFilter(string cadenaBuscar)
        {
            return _organizadorRepositorio.ObtenerPorFlitro(x => x.Apellido.Contains(cadenaBuscar), "Provincia, Ciudad")
                .Select(x => new OrganizadorDto()
                {
                    Id = x.Id,
                    Legajo = x.Legajo,
                    Apellido = x.Apellido,
                    Nombre = x.Nombre,
                    Cuil = x.Cuil,
                    Celular = x.Celular,
                    Estado = x.Estado,
                    Genero = x.Genero,
                    FechaNacimiento = x.FechaNacimiento,
                    Telefono = x.Telefono,
                    ProvinciaId = x.ProvinciaId,
                    ProvinciaStr = x.Provincia.Nombre,
                    CiudadId = x.CiudadId,
                    CiudadStr = x.Ciudad.Nombre
                }).ToList();
        }

        public OrganizadorDto GetById(long? Id)
        {
            var organizador = _organizadorRepositorio.ObtenerPorId(Id, "Provincia, Ciudad");

            return new OrganizadorDto()
            {
                Id = organizador.Id,
                Legajo = organizador.Legajo,
                Apellido = organizador.Apellido,
                Nombre = organizador.Nombre,
                Cuil = organizador.Cuil,
                Celular = organizador.Celular,
                Estado = organizador.Estado,
                Genero = organizador.Genero,
                FechaNacimiento = organizador.FechaNacimiento,
                Telefono = organizador.Telefono,
                ProvinciaId = organizador.ProvinciaId,
                ProvinciaStr = organizador.Provincia.Nombre,
                CiudadId = organizador.CiudadId,
                CiudadStr = organizador.Ciudad.Nombre

            };
        }

        public int ObtenerSiguienteLegajo()
        {
            var codigo = _organizadorRepositorio.ObtenerTodo();

            return codigo.Any() ? codigo.Max(x => x.Legajo) + 1 : 1;
        }

        public void Update(OrganizadorDto Dto)
        {
            var organizador = _organizadorRepositorio.ObtenerPorId(Dto.Id);

            organizador.Legajo = Dto.Legajo;
            organizador.Apellido = Dto.Apellido;
            organizador.Nombre = Dto.Nombre;
            organizador.Cuil = Dto.Cuil;
            organizador.Celular = Dto.Celular;
            organizador.Estado = Dto.Estado;
            organizador.Genero = Dto.Genero;
            organizador.FechaNacimiento = Dto.FechaNacimiento;
            organizador.Telefono = Dto.Telefono;
            organizador.ProvinciaId = Dto.ProvinciaId;
            organizador.CiudadId = Dto.CiudadId;

            _organizadorRepositorio.Modificar(organizador);
            _organizadorRepositorio.Save();
        }
    }
}
