﻿using IServicio.Sexo;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.SexoServicio
{
    public class SexoSevicio : ISexoServicio
    {
        public IEnumerable<SexoDto> ObtenerSexo()
        {
            return new List<SexoDto>()
            {
               new SexoDto() {Codigo = 1, Descripcion = "Masculino"},
               new SexoDto() {Codigo = 2, Descripcion = "Femenino"}

            }.ToList();
        }

    }
}
