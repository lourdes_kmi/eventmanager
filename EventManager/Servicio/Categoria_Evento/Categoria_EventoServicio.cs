﻿using Dominio.Repositorio.Categoria_Evento;
using IServicio.Categoria_Evento;
using IServicio.Categoria_Evento.Dto;
using Servicio.Base;
using System.Collections.Generic;
using System.Linq;

namespace Servicio.Categoria_Evento
{

    public class Categoria_EventoServicio : ServicioBase, ICategoria_EventoServicio
    {
        private readonly ICategoria_EventoRepositorio _categoria_EventoRepositorio;

        public Categoria_EventoServicio(ICategoria_EventoRepositorio categoria_EventoRepositorio)
        {
            _categoria_EventoRepositorio = categoria_EventoRepositorio;
        }

        public void Add(Categoria_EventoDto Dto)
        {
            var categoria = new Dominio.Entidades.Categoria_Evento
            {
                Descripcion = Dto.Descripcion,
                Estado = true,
                RowVersion = Dto.RowVersion
            };

            _categoria_EventoRepositorio.Agregar(categoria);
            _categoria_EventoRepositorio.Save();
        }

        public void Delete(long Id)
        {
            _categoria_EventoRepositorio.Eliminar(Id);
            _categoria_EventoRepositorio.Save();
        }

        public IEnumerable<Categoria_EventoDto> GetByFilter(string cadenaBuscar)
        {
            return _categoria_EventoRepositorio.ObtenerPorFlitro(x => x.Descripcion.Contains(cadenaBuscar))
            .Select(x => new Categoria_EventoDto
            {
                Id = x.Id,
                Descripcion = x.Descripcion,
                Estado = x.Estado,
                RowVersion = x.RowVersion

            }).ToList();

        }

        public Categoria_EventoDto GetById(long? Id)
        {
            var categoria = _categoria_EventoRepositorio.ObtenerPorId(Id);

            return new Categoria_EventoDto
            {
                Id = categoria.Id,
                Descripcion = categoria.Descripcion,
                Estado = categoria.Estado,
                RowVersion = categoria.RowVersion
            };
        }

        public void Update(Categoria_EventoDto Dto)
        {
            var categoria = _categoria_EventoRepositorio.ObtenerPorId(Dto.Id);

            categoria.Descripcion = Dto.Descripcion;
            categoria.Estado = Dto.Estado;

            _categoria_EventoRepositorio.Modificar(categoria);
            _categoria_EventoRepositorio.Save();
        }
    }
}
