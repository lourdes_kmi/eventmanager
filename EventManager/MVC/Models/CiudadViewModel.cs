﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace MVC.Models
{

    public class CiudadVm : EntidadBase
    {
        //Propiedades

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Nombre { get; set; }

        [Display(Name = "Codigo Postal")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DataType(DataType.PostalCode)]
        [Index("Ciudad_CP_Index", IsUnique = true)]
        public int CP { get; set; }

        [Display(Name = "Provincia")]
        public long ProvinciaId { get; set; }

        [Display(Name = "Provincia")]
        public string ProvinciaStr { get; set; }

        public bool EventosAsociados { get; set; }

        public IEnumerable<SelectListItem> Provincias { get; set; }

    }
}
