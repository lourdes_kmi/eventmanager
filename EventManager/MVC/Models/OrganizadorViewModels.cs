﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Models
{

    public class OrganizadorVm : PersonaVm
    {
        //propiedades

        [Index("Persona_Cuil_Index", IsUnique = true)]
        public int Legajo { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Nombre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Apellido { get; set; }

        [Display(Name = "Nombre")]
        public string ApyNom
        {
            get
            {
                return Nombre + " " + Apellido;
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(11, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [Index("Organizador_Cuil_Index", IsUnique = true)]
        public string Cuil { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        public string Genero { get; set; }

        [StringLength(11, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Telefono { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(10, ErrorMessage = "El campo {0} no debe superar los {1} caractere")]
        public string Celular { get; set; }

        public bool Estado { get; set; }

        [Display(Name = "Provincia")]
        public long ProvinciaId { get; set; }

        [Display(Name = "Provincia")]
        public string ProvinciaStr { get; set; }

        [Display(Name = "Ciudad")]
        public long CiudadId { get; set; }

        [Display(Name = "Ciudad")]
        public string CiudadStr { get; set; }

        public long? EventoId { get; set; } //se usa en evento asignarorganizador

        public string EstadoStr { get; set; }

        public bool EventosAsociados { get; set; }
    }
}
