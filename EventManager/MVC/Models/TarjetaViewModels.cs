﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MVC.Models
{
    public class TarjetaVm : EntidadBase
    {
        //Propiedades

        public string TipoTarjeta { get; set; } //Enum

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string NombreTitular { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        ////[CreditCard]
        public string Numero { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public int DniTitular { get; set; }

        public int CodigoSeguridad { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(5, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string FechaExpiracion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string BancoEmisor { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public int Cuotas { get; set; } //Enum

        public string TarjetaStr { get; set; }


    }
}
