﻿using System.ComponentModel.DataAnnotations;

namespace MVC.Models
{
    public class EntidadBase
    {
        [Key]
        public long Id { get; set; }
    }
}