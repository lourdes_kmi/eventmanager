﻿namespace MVC.Models
{

    public class FacturacionVm : EntidadBase
    {
        //Propiedades

        public long ProvinciaId { get; set; }

        public long CiudadId { get; set; }

        public int CodPostal { get; set; }

        public string Direccion { get; set; }

        public long TarjetaId { get; set; }

        public string TarjetaStr { get; set; }

    }
}
