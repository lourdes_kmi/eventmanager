﻿namespace MVC.Models
{
    public class PersonaEntradaVm
    {
        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Celular { get; set; }

        public string Direccion { get; set; }

    }
}