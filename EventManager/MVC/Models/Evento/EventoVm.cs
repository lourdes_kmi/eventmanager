﻿using MVC.Models.Entrada;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MVC.Models.Evento
{

    public class EventoVm : EntidadBase
    {
        //Propiedades

        public EventoVm()
        {
            Entradas = new List<EntradaVm>();

            Orador = new OradorVm();
        }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Imagen { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Fecha Inicio")]
        public DateTime FechaInicio { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Fecha Fin")]
        public DateTime FechaFin { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm}")]
        [Display(Name = "Hora Inicio")]
        public DateTime HoraInicio { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm}")]
        [Display(Name = "Hora Fin")]
        public DateTime HoraFin { get; set; }

        public string Lugar { get; set; }

        public string Calle { get; set; }

        [Range(1, 9999, ErrorMessage ="El valor no puede ser negativo")]
        public int Numero { get; set; }

        public string Referencias { get; set; }

        [Display(Name = "Organizador")]
        public long OrganizadorId { get; set; }

        [Display(Name = "Organizador")]
        public string OrganizadorStr { get; set; }

        [Display(Name = "Categoria")]
        public long Categoria_EventoId { get; set; }

        [Display(Name = "Categoria")]
        public string CategoriaStr { get; set; }

        [Display(Name = "Orador")]
        public long OradorId { get; set; }

        [Display(Name = "Orador")]
        public string OradorStr { get; set; }

        [Display(Name = "Provincia")]
        public long ProvinciaId { get; set; }

        [Display(Name = "Provincia")]
        public string ProvinciaStr { get; set; }

        [Display(Name = "Ciudad")]
        public long CiudadId { get; set; }

        [Display(Name = "Ciudad")]
        public string CiudadStr { get; set; }

        public string UsuarioNombre { get; set; }

        public List<EntradaVm> Entradas { get; set; }

        public EntradaVm EntradaTitulo { get; set; } // solo se usa para titulo de tablas, no usar ni asignar valores

        public OradorVm Orador { get; set; }

        public OrganizadorVm Organizador { get; set; }

        public bool TieneEntradas { get; set; }

    }
}
