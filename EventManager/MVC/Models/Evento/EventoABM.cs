﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace MVC.Models.Evento
{
    public class EventoABM : EntidadBase
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Nombre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(150, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }

        [Display(Name = "Buscar Imagen")]
        public HttpPostedFileBase BuscarImagen { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaInicio { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaFin { get; set; }

        [Required(ErrorMessage = "El campo {0} es obliatorio.")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime HoraInicio { get; set; }

        [Required(ErrorMessage = "El campo {0} es obliatorio.")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime HoraFin { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(120, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Lugar { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(70, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Calle { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [Range(1, 9999)]
        public int Numero { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(200, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [DataType(DataType.MultilineText)]
        public string Referencias { get; set; }

        [Display(Name = "Organizador")]
        public long OrganizadorId { get; set; }

        public IEnumerable<SelectListItem> Organizadores { get; set; }

        [Display(Name = "Categoria")]
        public long Categoria_EventoId { get; set; }

        public IEnumerable<SelectListItem> Categorias { get; set; }

        [Display(Name = "Orador")]
        public long OradorId { get; set; }

        public IEnumerable<SelectListItem> Oradores { get; set; }

        [Display(Name = "Provincia")]
        public long ProvinciaId { get; set; }

        public IEnumerable<SelectListItem> Provincias { get; set; }

        [Display(Name = "Ciudad")]
        public long CiudadId { get; set; }

        public IEnumerable<SelectListItem> Ciudades { get; set; }
    }
}