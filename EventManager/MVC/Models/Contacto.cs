﻿namespace MVC.Models
{

    public class Contacto : EntidadBase
    {
        //Propiedades

        public string Mail { get; set; }

        public string Telefono { get; set; }

        public string RedSocial1 { get; set; }

        public string RedSocial2 { get; set; }

        public long OrganizadorId { get; set; }

        public string OrganizadorStr { get; set; }

    }
}
