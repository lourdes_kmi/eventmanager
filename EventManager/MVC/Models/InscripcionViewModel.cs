﻿using IServicio.Entrada.Dto;
using IServicio.Evento.Dto;
using IServicio.Faturacion.Dto;
using IServicio.LineaResumen.Dto;
using IServicio.Oyente.Dto;
using IServicio.Resumen.Dto;
using IServicio.Tarjeta.Dto;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Models
{

    public class InscripcionViewModel : EntidadBase
    {
        public InscripcionViewModel()
        {
            Entradas = new List<EntradaDto>();
            LineaResumenes = new List<LineaResumenDto>();
        }

        public DateTime FechaInscripcion { get; set; }

        public long EventoId { get; set; }

        public string CodigoInscripcion { get; set; }

        public EventoDto Evento { get; set; }

        public TarjetaDto Tarjeta { get; set; }

        public OyenteDto Oyente { get; set; }

        public ResumenDto Resumen { get; set; }

        public FacturacionDto Facturacion { get; set; }

        public List<EntradaDto> Entradas { get; set; }

        public List<LineaResumenDto> LineaResumenes { get; set; }

        public EntradaDto Entrada { get; set; } // se usa solo para titulos de tabla de entradas

        public string Subtotal { get; set; } // se usa solo para titulo "Subtotal" de la tabla resumen pedido

        public IEnumerable<SelectListItem> Provincias { get; set; }

        public IEnumerable<SelectListItem> Ciudades { get; set; }
    }
}
