﻿using System.Collections.Generic;

namespace MVC.Models
{
    public class ResumenViewModel : EntidadBase
    {
        public ResumenViewModel()
        {
            LineaResumenes = new List<LineaResumenViewModel>();
        }

        //Propiedades

        public decimal? Total { get; set; }

        //Propiedades de Navegacion

        public List<LineaResumenViewModel> LineaResumenes { get; set; } 
    }
}
