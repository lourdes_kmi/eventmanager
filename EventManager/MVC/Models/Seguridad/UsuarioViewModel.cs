﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MVC.Models.Seguridad
{

    public class UsuarioViewModel
    {
        //Propiedades

        public UsuarioViewModel()
        {
            Roles = new List<RoleView>();
        }

        [Display(Name = "Id")]
        public string UserId { get; set; }

        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public RoleView Role { get; set; }

        public List<RoleView> Roles { get; set; }
    }
}
