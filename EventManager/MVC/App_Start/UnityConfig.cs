using Dominio.Repositorio.Categoria_Evento;
using Dominio.Repositorio.Ciudad;
using Dominio.Repositorio.Contacto;
using Dominio.Repositorio.Entrada;
using Dominio.Repositorio.Evento;
using Dominio.Repositorio.Facturacion;
using Dominio.Repositorio.Inscripcion;
using Dominio.Repositorio.LineaResumen;
using Dominio.Repositorio.ListaBancos;
using Dominio.Repositorio.Orador;
using Dominio.Repositorio.Organizador;
using Dominio.Repositorio.Oyente;
using Dominio.Repositorio.Persona;
using Dominio.Repositorio.Provincia;
using Dominio.Repositorio.Resumen;
using Dominio.Repositorio.Tarjeta;
using Dominio.Repositorio.TipoTarjeta;
using Infraestructura.Repositorio.Categoria_Evento;
using Infraestructura.Repositorio.Ciudad;
using Infraestructura.Repositorio.Contacto;
using Infraestructura.Repositorio.Entrada;
using Infraestructura.Repositorio.Evento;
using Infraestructura.Repositorio.Facturacion;
using Infraestructura.Repositorio.Inscripcion;
using Infraestructura.Repositorio.LineaResumen;
using Infraestructura.Repositorio.ListaBanco;
using Infraestructura.Repositorio.Orador;
using Infraestructura.Repositorio.Organizador;
using Infraestructura.Repositorio.Oyente;
using Infraestructura.Repositorio.Persona;
using Infraestructura.Repositorio.Provincia;
using Infraestructura.Repositorio.Resumen;
using Infraestructura.Repositorio.Tarjeta;
using Infraestructura.Repositorio.TipoTarjeta;
using IServicio.Categoria_Evento;
using IServicio.Ciudad;
using IServicio.Contacto;
using IServicio.Entrada;
using IServicio.Evento;
using IServicio.Facturacion;
using IServicio.Inscripcion;
using IServicio.LineaResumen;
using IServicio.ListaBancos;
using IServicio.Orador;
using IServicio.Organizador;
using IServicio.Oyente;
using IServicio.Persona;
using IServicio.Provincia;
using IServicio.Resumen;
using IServicio.Sexo;
using IServicio.Tarjeta;
using IServicio.TipoTarjeta;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using MVC.Controllers;
using MVC.Helpers.ComboBox.Categoria;
using MVC.Helpers.ComboBox.Ciudad;
using MVC.Helpers.ComboBox.Orador;
using MVC.Helpers.ComboBox.Organizador;
using MVC.Helpers.ComboBox.Provincia;
using MVC.Helpers.ComboBox.Sexo;
using MVC.Models;
using Servicio.Categoria_Evento;
using Servicio.Ciudad;
using Servicio.Contacto;
using Servicio.Entrada;
using Servicio.Evento;
using Servicio.Facturacion;
using Servicio.Inscripcion;
using Servicio.LineaResumen;
using Servicio.ListaBancos;
using Servicio.Orador;
using Servicio.Organizador;
using Servicio.Oyente;
using Servicio.Persona;
using Servicio.Provincia;
using Servicio.Resumen;
using Servicio.SexoServicio;
using Servicio.Tarjeta;
using Servicio.TipoTarjeta;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Unity.Mvc5;

namespace MVC
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            container.RegisterType<IAuthenticationManager>(
                new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));

            container.RegisterType<AccountController>(
                new InjectionConstructor(typeof(ApplicationUserManager), typeof(ApplicationSignInManager)));

            container.RegisterType<AccountController>(new InjectionConstructor());

            container.RegisterType<UserManager<ApplicationUser>>(new HierarchicalLifetimeManager());

            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(
                new HierarchicalLifetimeManager());

            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());



            //-----------------------------------Servicios----------------------------------------------

            container.RegisterType<IPersonaServicio, PersonaServicio>();
            container.RegisterType<IPersonaRepositorio, PersonaRepositorio>();

            container.RegisterType<ICategoria_EventoServicio, Categoria_EventoServicio>();
            container.RegisterType<ICategoria_EventoRepositorio, Categoria_EventoRepositorio>();

            container.RegisterType<ICiudadServicio, CiudadServicio>();
            container.RegisterType<ICiudadRepositorio, CiudadRepositorio>();

            container.RegisterType<IProvinciaServicio, ProvinciaServicio>();
            container.RegisterType<IProvinciaRepositorio, ProvinciaRepositorio>();

            container.RegisterType<IOrganizadorServicio, OrganizadorServicio>();
            container.RegisterType<IOrganizadorRepositorio, OrganizadorRepositorio>();

            container.RegisterType<IOyenteServicio, OyenteServicio>();
            container.RegisterType<IOyenteRepositorio, OyenteRepositorio>();

            container.RegisterType<IOradorServicio, OradorServicio>();
            container.RegisterType<IOradorRepositorio, OradorRepositorio>();

            container.RegisterType<IFacturacionServicio, FacturacionServicio>();
            container.RegisterType<IFacturacionRepositorio, FacturacionRepositorio>();

            container.RegisterType<ILineaResumenServicio, LineaResumenServicio>();
            container.RegisterType<ILineaResumenRepositorio, LineaResumenRepositorio>();

            container.RegisterType<IEntradaServicio, EntradaServicio>();
            container.RegisterType<IEntradaRepositorio, EntradaRepositorio>();

            container.RegisterType<IContactoServicio, ContactoServicio>();
            container.RegisterType<IContactoRepositorio, ContactoRepositorio>();

            container.RegisterType<ITarjetaServicio, TarjetaServicio>();
            container.RegisterType<ITarjetaRepositorio, TarjetaRepositorio>();

            container.RegisterType<IInscripcionServicio, InscripcionServicio>();
            container.RegisterType<IInscripcionRepositorio, InscripcionRepositorio>();

            container.RegisterType<IEventoServicio, EventoServicio>();
            container.RegisterType<IEventoRepositorio, EventoRepositorio>();

            container.RegisterType<IResumenServicio, ResumenServicio>();
            container.RegisterType<IResumenRepositorio, ResumenRepositorio>();

            container.RegisterType<ISexoServicio, SexoSevicio>();

            container.RegisterType<IListaBancosServicio, ListaBancosServicio>();
            container.RegisterType<IListaBancosRepositorio, ListaBancosRepositorio>();

            container.RegisterType<ITipoTarjetaServicio, TipoTarjetaServicio>();
            container.RegisterType<ITipoTarjetaRepositorio, TipoTarjetaRepositorio>();

            

            //-----------------------------------ComboBox----------------------------------------------

            container.RegisterType<IComboBoxCategoria, ComboBoxCategoria>();

            container.RegisterType<IComboBoxOrganizador, ComboBoxOrganizador>();

            container.RegisterType<IComboBoxOrador, ComboBoxOrador>();

            container.RegisterType<IComboBoxProvincia, ComboBoxProvincia>();

            container.RegisterType<IComboBoxCiudad, ComboBoxCiudad>();

            container.RegisterType<IComboBoxSexo, ComboBoxSexo>();

        }


    }
}