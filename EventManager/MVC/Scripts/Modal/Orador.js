﻿$(".btnCambiar").click(function (eve) {
    $("#modal-content").load("/Orador/AsignarOrador/" + $(this).data("id"));
});
$("#btnCrearOrador").click(function (eve) {
    $("#modal-content").load("/Orador/Create");
});

$(".btnEditarOrador").click(function (eve) {
    $("#modal-content").load("/Orador/Edit/" + $(this).data("id"));
});

$(".btnDetalleOrador").click(function (eve) {
    $("#modal-content").load("/Orador/Details/" + $(this).data("id"));
});

$(".btnEliminarOrador").click(function (eve) {
    $("#modal-content").load("/Orador/Delete/" + $(this).data("id"));
});