﻿$("#btnNuevo").click(function (eve) {
    $("#modal-content").load("/Categoria_Evento/Create");
});

$(".btnEditar").click(function (eve) {
    $("#modal-content").load("/Categoria_Evento/Edit/" + $(this).data("id"));
});

$(".btnDetalle").click(function (eve) {
    $("#modal-content").load("/Categoria_Evento/Details/" + $(this).data("id"));
});

$(".btnEliminar").click(function (eve) {
    $("#modal-content").load("/Categoria_Evento/Delete/" + $(this).data("id"));
});