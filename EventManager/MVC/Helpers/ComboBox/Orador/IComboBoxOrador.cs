﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Orador
{
    public interface IComboBoxOrador
    {
        IEnumerable<SelectListItem> Poblar();
    }
}