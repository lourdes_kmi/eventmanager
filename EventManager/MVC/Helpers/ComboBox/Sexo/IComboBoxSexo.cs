﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Sexo
{
    public interface IComboBoxSexo
    {
        IEnumerable<SelectListItem> Poblar();
    }

}