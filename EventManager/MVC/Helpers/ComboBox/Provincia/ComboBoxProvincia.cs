﻿using IServicio.Provincia;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Provincia
{
    public class ComboBoxProvincia : IComboBoxProvincia
    {
        private readonly IProvinciaServicio _provinciaServicio;

        public ComboBoxProvincia(IProvinciaServicio provinciaServicio)
        {
            _provinciaServicio = provinciaServicio;
        }

        IEnumerable<SelectListItem> IComboBoxProvincia.Poblar()
        {
            var provincia = _provinciaServicio.GetByFilter(string.Empty);

            return new SelectList(provincia, "Id", "Nombre");
        }
    }
}