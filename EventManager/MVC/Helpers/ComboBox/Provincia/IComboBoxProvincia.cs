﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MVC.Helpers.ComboBox.Provincia
{
    public interface IComboBoxProvincia
    {
        IEnumerable<SelectListItem> Poblar();
    }
}