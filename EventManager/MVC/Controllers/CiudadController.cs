﻿using Infraestructura;
using IServicio.Ciudad;
using IServicio.Ciudad.Dto;
using IServicio.Evento;
using MVC.Helpers.ComboBox.Provincia;
using MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class CiudadController : Controller
    {
        private EventManagerContext db = new EventManagerContext();

        private readonly ICiudadServicio _ciudadServicio;
        private readonly IComboBoxProvincia _comboBoxProvincia;
        private readonly IEventoServicio _eventoServicio;

        public CiudadController()
        {

        }

        public CiudadController(ICiudadServicio ciudadServicio, IComboBoxProvincia comboBoxProvincia, IEventoServicio eventoServicio)
        {
            _ciudadServicio = ciudadServicio;
            _comboBoxProvincia = comboBoxProvincia;
            _eventoServicio = eventoServicio;
        }

        // GET: Ciudads
        public ActionResult Index(string cadenaBuscar)
        {
            var ciudades = _ciudadServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            var lista = new List<CiudadVm>();

            foreach (var item in ciudades)
            {
                var eventos = _eventoServicio.GetAll().Where(x => x.OradorId == item.Id);

                if (eventos.Count() > 0)
                {
                    var ciudad = new CiudadVm()
                    {
                        Id = item.Id,
                        Nombre = item.Nombre,
                        CP = item.CP,
                        ProvinciaId = item.ProvinciaId,
                        ProvinciaStr = item.ProvinciaStr,
                        EventosAsociados = true
                    };

                    lista.Add(ciudad);
                }

                else
                {
                    var ciudad = new CiudadVm()
                    {
                        Id = item.Id,
                        Nombre = item.Nombre,
                        CP = item.CP,
                        ProvinciaId = item.ProvinciaId,
                        ProvinciaStr = item.ProvinciaStr,
                        EventosAsociados = false
                    };

                    lista.Add(ciudad);
                }
            }

            return View(lista.Select(x => new CiudadVm()
            {
                Id = x.Id,
                Nombre = x.Nombre,
                CP = x.CP,
                ProvinciaStr = x.ProvinciaStr,
                EventosAsociados = x.EventosAsociados
            }).ToList());
        }

        // GET: Ciudad/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var ciudad = _ciudadServicio.GetById(id);

            if (ciudad == null)
            {
                return HttpNotFound();
            }

            return View(new CiudadVm()
            {
                Id = ciudad.Id,
                Nombre = ciudad.Nombre,
                CP = ciudad.CP,
                ProvinciaStr = ciudad.ProvinciaStr
            });
        }

        // GET: Ciudad/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View(new CiudadVm()
            {
                Provincias = _comboBoxProvincia.Poblar()
            });
        }

        // POST: Ciudad/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CiudadVm ciudadVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var ciudadDto = ConvertirCiudadDto(ciudadVm);

                    _ciudadServicio.Add(ciudadDto);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                return View(new CiudadVm()
                {
                    Provincias = _comboBoxProvincia.Poblar()
                });
            }

            return RedirectToAction("Index");
        }



        // GET: Ciudad/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var ciudad = _ciudadServicio.GetById(id);

            if (ciudad == null)
            {
                return HttpNotFound();
            }

            return View(new CiudadVm()
            {
                Provincias = _comboBoxProvincia.Poblar(),

                Nombre = ciudad.Nombre,
                CP = ciudad.CP,
                ProvinciaId = ciudad.ProvinciaId
            });
        }



        // POST: Ciudad/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CiudadVm ciudadVm)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var ciudadDto = ConvertirCiudadDto(ciudadVm);

                    _ciudadServicio.Update(ciudadDto);

                }

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                return View(new CiudadVm()
                {
                    Provincias = _comboBoxProvincia.Poblar()
                });
            }

            return RedirectToAction("Index");

        }

        // GET: Ciudad/Delete/5
        public ActionResult Delete(long? id, CiudadVm ciudadVm)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var ciudad = _ciudadServicio.GetById(id);

            if (ciudad == null)
            {
                return HttpNotFound();
            }
            return View(new CiudadVm()
            {
                Nombre = ciudad.Nombre,
                CP = ciudad.CP,
                ProvinciaStr = ciudad.ProvinciaStr
            });
        }

        // POST: Ciudad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id, CiudadVm ciudadVm)
        {
            try
            {
                _ciudadServicio.Delete(id);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "No se puede borrar, existen registros relacionados");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //Metodos Privados

        private static CiudadDto ConvertirCiudadDto(CiudadVm ciudadVm)
        {
            return new CiudadDto()
            {
                Id = ciudadVm.Id,
                Nombre = ciudadVm.Nombre,
                CP = ciudadVm.CP,
                ProvinciaId = ciudadVm.ProvinciaId
            };
        }
    }
}
