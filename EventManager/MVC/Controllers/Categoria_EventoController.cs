﻿using Infraestructura;
using IServicio.Categoria_Evento;
using IServicio.Categoria_Evento.Dto;
using IServicio.Evento;
using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class Categoria_EventoController : Controller
    {
        private EventManagerContext db = new EventManagerContext();
        private readonly ICategoria_EventoServicio _categoria_EventoServicio;
        private readonly IEventoServicio _eventoServicio;

        public Categoria_EventoController()
        {

        }

        public Categoria_EventoController(ICategoria_EventoServicio categoria_EventoServicio, IEventoServicio eventoServicio)
        {
            _categoria_EventoServicio = categoria_EventoServicio;
            _eventoServicio = eventoServicio;
        }

        // GET: Categoria_Evento
        public ActionResult Index(string cadenaBuscar)
        {
            var categorias = _categoria_EventoServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            var lista = new List<Categoria_EventoVm>();

            foreach (var item in categorias)
            {
                var eventos = _eventoServicio.GetAll().Where(x => x.OradorId == item.Id);

                if (eventos.Count() > 0)
                {
                    var categoria = new Categoria_EventoVm()
                    {
                        Id = item.Id,
                        Descripcion = item.Descripcion,
                        Estado = item.Estado,
                        EventosAsociados = true
                    };

                    lista.Add(categoria);
                }

                else
                {
                    var categoria = new Categoria_EventoVm()
                    {
                        Id = item.Id,
                        Descripcion = item.Descripcion,
                        Estado = item.Estado,
                        EventosAsociados = false
                    };

                    lista.Add(categoria);
                }
            }

            return View(lista.Select(x => new Categoria_EventoVm()
            {
                Id = x.Id,
                Descripcion = x.Descripcion,
                Estado = x.Estado,
                EstadoStr = x.Estado ? "Activo" : "Inactivo",
                EventosAsociados = x.EventosAsociados
            }).ToList());
        }

        // GET: Categoria_Evento/Details/5
        public ActionResult Details(long? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var categoria = _categoria_EventoServicio.GetById(id);

            if (categoria == null)
            {
                return HttpNotFound();
            }
            return View(new Categoria_EventoVm()
            {
                Id = categoria.Id,
                Descripcion = categoria.Descripcion,
                Estado = categoria.Estado

            });
        }

        // GET: Categoria_Evento/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categoria_Evento/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Descripcion,Estado")] Categoria_EventoVm categoria_EventoVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var CategoriaDto = CargarCategoriaDto(categoria_EventoVm);

                    _categoria_EventoServicio.Add(CategoriaDto);
                }
            }
            catch (System.Exception)
            {

                return View(categoria_EventoVm);
            }

            return RedirectToAction("Index");
        }

        // GET: Categoria_Evento/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var categoria = _categoria_EventoServicio.GetById(id);

            if (categoria == null)
            {
                return HttpNotFound();
            }
            return View(new Categoria_EventoVm()
            {
                Descripcion = categoria.Descripcion,
                Estado = categoria.Estado
            });
        }

        // POST: Categoria_Evento/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Descripcion,Estado")] Categoria_EventoVm categoria_EventoVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var categoriaDto = CargarCategoriaDto(categoria_EventoVm);
                    _categoria_EventoServicio.Update(categoriaDto);
                }
            }
            catch
            {

                return View(categoria_EventoVm);
            }

            return RedirectToAction("Index");
        }

        // GET: Categoria_Evento/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var categoria = _categoria_EventoServicio.GetById(id);

            if (categoria == null)
            {
                return HttpNotFound();
            }

            return View(new Categoria_EventoVm()
            {
                Descripcion = categoria.Descripcion,
                Estado = categoria.Estado
            });
        }

        // POST: Categoria_Evento/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id, Categoria_EventoVm CategoriaVm)
        {
            try
            {
                var categoria = _categoria_EventoServicio.GetById(id);

                if (categoria == null) return HttpNotFound();

                _categoria_EventoServicio.Delete(id);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return View(CategoriaVm);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //Metodos privados

        private static Categoria_EventoDto CargarCategoriaDto(Categoria_EventoVm categoria_EventoVm)
        {
            return new Categoria_EventoDto()
            {
                Id = categoria_EventoVm.Id,
                Descripcion = categoria_EventoVm.Descripcion
            };
        }
    }
}
