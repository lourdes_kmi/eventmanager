﻿using IServicio.Entrada;
using IServicio.LineaResumen;
using IServicio.Organizador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class DetalleResumenController : Controller
    {
        private readonly ILineaResumenServicio _lineaResumenServicio;
        private readonly IEntradaServicio _entradaServicio;
        private readonly IOrganizadorServicio _organizadorServicio;

        public DetalleResumenController()
        {

        }

        public DetalleResumenController(ILineaResumenServicio lineaResumenServicio, IEntradaServicio entradaServicio
                                        , IOrganizadorServicio organizadorServicio)
        {
            _lineaResumenServicio = lineaResumenServicio;
            _entradaServicio = entradaServicio;
            _organizadorServicio = organizadorServicio;
        }

        // GET: DetalleResumen
        public ActionResult Index()
        {
            return View();
        }

        // GET: DetalleResumen/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: DetalleResumen/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DetalleResumen/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DetalleResumen/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DetalleResumen/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DetalleResumen/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DetalleResumen/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
