﻿using Infraestructura;
using IServicio.Persona;
using IServicio.Persona.Dto;
using MVC.Models;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class PersonaController : Controller
    {
        protected EventManagerContext db = new EventManagerContext();
        private readonly IPersonaServicio personaServicio;

        public PersonaController()
        {

        }

        public PersonaController(IPersonaServicio persona)
        {
            personaServicio = persona;
        }

        // GET: Persona
        public ActionResult Index(string cadenaBuscar)
        {
            var persona = personaServicio.GetByFilter((!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty));

            return View(persona.Select(x => new PersonaVm()
            {
                Id = x.Id,
                Nombre = x.Nombre,
                Apellido = x.Apellido,
                Celular = x.Celular,
                Cuil = x.Cuil,
                FechaNacimiento = x.FechaNacimiento,
                Genero = x.Genero,
                Telefono = x.Telefono,
                CiudadId = x.CiudadId,
                ProvinciaId = x.ProvinciaId,
                ProvinciaStr = x.ProvinciaStr

            }).ToList());
        }

        // GET: Persona/Details/5
        public ActionResult Details(long? id)
        {
            var persona = personaServicio.GetById(id);

            return View(new PersonaVm()
            {
                Nombre = persona.Nombre,
                Apellido = persona.Apellido,
                Celular = persona.Celular,
                Cuil = persona.Cuil,
                FechaNacimiento = persona.FechaNacimiento,
                Genero = persona.Genero,
                Telefono = persona.Telefono,
                CiudadId = persona.CiudadId,
                ProvinciaId = persona.ProvinciaId
            });
        }

        // GET: Persona/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Persona/Create
        [HttpPost]
        public ActionResult Create(PersonaVm personaVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var personaDto = CargarPersonaDto(personaVm);

                    personaServicio.Add(personaDto);
                }

            }
            catch
            {
                return View(personaVm);
            }

            return RedirectToAction("Index");
        }

        // GET: Persona/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var personaDto = personaServicio.GetById(id.Value);

            if (personaDto == null) return HttpNotFound();

            return View(new PersonaVm()
            {
                Nombre = personaDto.Nombre,
                Apellido = personaDto.Apellido,
                Celular = personaDto.Celular,
                Cuil = personaDto.Cuil,
                FechaNacimiento = personaDto.FechaNacimiento,
                Genero = personaDto.Genero,
                Telefono = personaDto.Telefono,
                CiudadId = personaDto.CiudadId,
                ProvinciaId = personaDto.ProvinciaId
            });
        }

        // POST: Persona/Edit/5
        [HttpPost]
        public ActionResult Edit(PersonaVm personaVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var alumnoDto = CargarPersonaDto(personaVm);

                    personaServicio.Update(alumnoDto);
                }


            }
            catch
            {
                return View(personaVm);
            }

            return RedirectToAction("Index");
        }

        // GET: Persona/Delete/5
        public ActionResult Delete(long? id)
        {
            var persona = personaServicio.GetById(id);

            return View(new PersonaVm()
            {
                Nombre = persona.Nombre,
                Apellido = persona.Apellido,
                Celular = persona.Celular,
                Cuil = persona.Cuil,
                FechaNacimiento = persona.FechaNacimiento,
                Genero = persona.Genero,
                Telefono = persona.Telefono,
                CiudadId = persona.CiudadId,
                ProvinciaId = persona.ProvinciaId
            });
        }

        // POST: Persona/Delete/5
        [HttpPost]
        public ActionResult Delete(long id, PersonaVm PersonaVm)
        {
            try
            {
                var persona = personaServicio.GetById(id);

                if (persona == null) return HttpNotFound();

                personaServicio.Delete(id);
            }
            catch
            {
                return View(PersonaVm);
            }

            return RedirectToAction("Index");
        }

        //Metodos privados

        private static PersonaDto CargarPersonaDto(PersonaVm personaVm)
        {
            return new PersonaDto()
            {
                Id = personaVm.Id,
                Genero = personaVm.Genero,
                Apellido = personaVm.Apellido,
                Nombre = personaVm.Nombre,
                Celular = personaVm.Celular,
                Telefono = personaVm.Telefono,
                FechaNacimiento = personaVm.FechaNacimiento,
                Cuil = personaVm.Cuil,
                CiudadId = personaVm.CiudadId,
                ProvinciaId = personaVm.ProvinciaId
            };
        }
    }
}
