﻿using Infraestructura;
using IServicio.Ciudad;
using IServicio.Evento;
using IServicio.Organizador;
using IServicio.Organizador.Dto;
using MVC.Helpers.ComboBox.Ciudad;
using MVC.Helpers.ComboBox.Provincia;
using MVC.Helpers.ComboBox.Sexo;
using MVC.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class OrganizadorController : Controller
    {
        private EventManagerContext db = new EventManagerContext();

        private readonly IOrganizadorServicio _organizadorServicio;
        private readonly IEventoServicio _eventoServicio;
        private readonly IComboBoxProvincia _comboBoxProvincia;
        private readonly ICiudadServicio _ciudadServicio;
        private readonly IComboBoxCiudad _comboBoxCiudad;
        private readonly IComboBoxSexo _comboBoxSexo;

        public OrganizadorController()
        {

        }

        public OrganizadorController(IOrganizadorServicio organizadorServicio, IEventoServicio eventoServicio
                                    , IComboBoxProvincia comboBoxProvincia, IComboBoxCiudad comboBoxCiudad
                                    , IComboBoxSexo comboBoxSexo, ICiudadServicio ciudadServicio)
        {
            _organizadorServicio = organizadorServicio;
            _eventoServicio = eventoServicio;
            _comboBoxProvincia = comboBoxProvincia;
            _comboBoxCiudad = comboBoxCiudad;
            _comboBoxSexo = comboBoxSexo;
            _ciudadServicio = ciudadServicio;
        }

        // GET: Organizador
        public ActionResult Index(string cadenaBuscar)
        {
            var organizadores = _organizadorServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            var lista = new List<OrganizadorVm>();

            foreach (var item in organizadores)
            {
                var eventos = _eventoServicio.GetAll().Where(x => x.OrganizadorId == item.Id);

                if (eventos.Count() > 0)
                {
                    var orador = new OrganizadorVm()
                    {
                        Id = item.Id,
                        Legajo = item.Legajo,
                        Apellido = item.Apellido,
                        Nombre = item.Nombre,
                        Celular = item.Celular,
                        FechaNacimiento = item.FechaNacimiento,
                        Genero = item.Genero,
                        Cuil = item.Cuil,
                        Estado = item.Estado,
                        EstadoStr = item.Estado ? "Activo" : "Inactivo",
                        Telefono = item.Telefono,
                        ProvinciaStr = item.ProvinciaStr,
                        CiudadStr = item.CiudadStr,
                        EventosAsociados = true
                    };

                    lista.Add(orador);
                }

                else
                {
                    var orador = new OrganizadorVm()
                    {
                        Id = item.Id,
                        Legajo = item.Legajo,
                        Apellido = item.Apellido,
                        Nombre = item.Nombre,
                        Celular = item.Celular,
                        FechaNacimiento = item.FechaNacimiento,
                        Genero = item.Genero,
                        Cuil = item.Cuil,
                        Estado = item.Estado,
                        EstadoStr = item.Estado ? "Activo" : "Inactivo",
                        Telefono = item.Telefono,
                        ProvinciaStr = item.ProvinciaStr,
                        CiudadStr = item.CiudadStr,
                        EventosAsociados = false
                    };

                    lista.Add(orador);
                }
            }

            return View(lista.Select(x => new OrganizadorVm()
            {
                Id = x.Id,
                Apellido = x.Apellido,
                Nombre = x.Nombre,
                Celular = x.Celular,
                FechaNacimiento = x.FechaNacimiento,
                Genero = x.Genero,
                Legajo = x.Legajo,
                Cuil = x.Cuil,
                Estado = x.Estado,
                EstadoStr = x.Estado ? "Activo" : "Inactivo",
                Telefono = x.Telefono,
                ProvinciaStr = x.ProvinciaStr,
                CiudadStr = x.CiudadStr,
                EventosAsociados = x.EventosAsociados
            }).ToList());
        }

        // GET: Organizador/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var organizador = _organizadorServicio.GetById(id);

            if (organizador == null)
            {
                return HttpNotFound();
            }
            return View(new OrganizadorVm()
            {
                Id = organizador.Id,
                Apellido = organizador.Apellido,
                Nombre = organizador.Nombre,
                Celular = organizador.Celular,
                FechaNacimiento = organizador.FechaNacimiento,
                Genero = organizador.Genero,
                Legajo = organizador.Legajo,
                Cuil = organizador.Cuil,
                Estado = organizador.Estado,
                Telefono = organizador.Telefono,
                ProvinciaStr = organizador.ProvinciaStr,
                CiudadStr = organizador.CiudadStr
            });
        }

        // GET: Organizador/Create
        [HttpGet]
        public ActionResult Create()
        {
            List<SelectListItem> listaCiudad = new List<SelectListItem>()
            {
            };

            return View(new OrganizadorVm()
            {
                Provincias = _comboBoxProvincia.Poblar(),
                Ciudades = listaCiudad,
                Sexos = _comboBoxSexo.Poblar()
            });
        }

        // POST: Organizador/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrganizadorVm organizadorVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var organizadorDto = cargarOrganizadorDto(organizadorVm);

                    _organizadorServicio.Add(organizadorDto);
                }

                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                List<SelectListItem> listaCiudad = new List<SelectListItem>()
                {
                };

                return View(new OrganizadorVm
                {
                    Sexos = _comboBoxSexo.Poblar(),
                    Provincias = _comboBoxProvincia.Poblar(),
                    Ciudades = listaCiudad
                });
            }

           
        }

        // GET: Organizador/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var organizador = _organizadorServicio.GetById(id);

            if (organizador == null)
            {
                return HttpNotFound();
            }

            return View(new OrganizadorVm()
            {
                Provincias = _comboBoxProvincia.Poblar(),
                Ciudades = _comboBoxCiudad.Poblar(),
                Sexos = _comboBoxSexo.Poblar(),
                Apellido = organizador.Apellido,
                Nombre = organizador.Nombre,
                Celular = organizador.Celular,
                FechaNacimiento = organizador.FechaNacimiento,
                Genero = organizador.Genero,
                Legajo = organizador.Legajo,
                Cuil = organizador.Cuil,
                Estado = organizador.Estado,
                Telefono = organizador.Telefono,
                ProvinciaId = organizador.ProvinciaId,
                CiudadId = organizador.CiudadId
            });
        }

        // POST: Organizador/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OrganizadorVm organizadorVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var organizadorDto = cargarOrganizadorDto(organizadorVm);

                    organizadorDto.Estado = organizadorVm.Estado;

                    _organizadorServicio.Update(organizadorDto);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                            ex.InnerException.InnerException != null &&
                            ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            return View(new OrganizadorVm
            {
                Sexos = _comboBoxSexo.Poblar(),
                Provincias = _comboBoxProvincia.Poblar(),
                Ciudades = _comboBoxCiudad.Poblar()
            });
        }

        // GET: Organizador/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var organizador = _organizadorServicio.GetById(id);

            if (organizador == null)
            {
                return HttpNotFound();
            }

            return View(new OrganizadorVm()
            {
                Apellido = organizador.Apellido,
                Nombre = organizador.Nombre,
                Celular = organizador.Celular,
                FechaNacimiento = organizador.FechaNacimiento,
                Genero = organizador.Genero,
                Legajo = organizador.Legajo,
                Cuil = organizador.Cuil,
                Estado = organizador.Estado,
                Telefono = organizador.Telefono,
                ProvinciaStr = organizador.ProvinciaStr,
                CiudadStr = organizador.CiudadStr
            });
        }

        // POST: Organizador/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id, OrganizadorVm organizadorVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _organizadorServicio.Delete(id);
                    
                }
            }


            catch (Exception ex)
            {
                if (ex.InnerException != null &&
               ex.InnerException.InnerException != null &&
               ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "No se puede borrar, existen registros relacionados");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            return RedirectToAction("Index");

        }

        public ActionResult AsignarOrganizador(long id, string cadenaBuscar)
        {
           var eventoId = id;

            var organizador = _organizadorServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            ViewBag.EventoId = eventoId;

            return View(organizador.Select(x => new OrganizadorVm()
            {
                Id = x.Id,
                Legajo = x.Legajo,
                Apellido = x.Apellido,
                Nombre = x.Nombre,
                Celular = x.Celular,
                FechaNacimiento = x.FechaNacimiento,
                Genero = x.Genero,
                Cuil = x.Cuil,
                Estado = x.Estado,
                EstadoStr = x.Estado == true ? "Activo" : "Inactivo",
                Telefono = x.Telefono,
                ProvinciaStr = x.ProvinciaStr,
                CiudadStr = x.CiudadStr,
                EventoId = eventoId

            }).ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AsignarOrganizador(long organizadorId, long? eventoId, OrganizadorVm organizadorVm)
        {

            try
            {
                _eventoServicio.AsignarOrganizador(eventoId, organizadorId);
            }
            catch (Exception ex)
            {

                return View(organizadorVm);
            }

            return RedirectToAction("AdministrarEvento", "Evento", new { id = eventoId });
        }

        /// <summary>
        /// Reportes(PDF)
        /// </summary>
        /// <returns></returns>
        public ActionResult Reporte(string cadenaBuscar)
        {
            var organizador = _organizadorServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            return View(organizador.Select(x => new OrganizadorVm()
            {
                Id = x.Id,
                Apellido = x.Apellido,
                Nombre = x.Nombre,
                Celular = x.Celular,
                FechaNacimiento = x.FechaNacimiento,
                Genero = x.Genero,
                Legajo = x.Legajo,
                Cuil = x.Cuil,
                Estado = x.Estado,
                EstadoStr = x.Estado ? "Activo" : "Inactivo",
                Telefono = x.Telefono,
                ProvinciaStr = x.ProvinciaStr,
                CiudadStr = x.CiudadStr
            }).ToList());
        }

        public ActionResult Imprimir()
        {
            return new ActionAsPdf("Reporte") { FileName = "Prueba.pdf" };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //metodos privados

        private OrganizadorDto cargarOrganizadorDto(OrganizadorVm organizadorVm)
        {
            return new OrganizadorDto()
            {
                Id = organizadorVm.Id,
                Apellido = organizadorVm.Apellido,
                Nombre = organizadorVm.Nombre,
                Celular = organizadorVm.Celular,
                FechaNacimiento = organizadorVm.FechaNacimiento,
                Genero = organizadorVm.Genero == "1" ? "Masculino" : "Femenino",
                Legajo = _organizadorServicio.ObtenerSiguienteLegajo(),
                Cuil = organizadorVm.Cuil,
                Telefono = organizadorVm.Telefono,
                ProvinciaId = organizadorVm.ProvinciaId,
                CiudadId = organizadorVm.CiudadId
            };
        }

        public JsonResult ObtenerCiudades(long provinciaId)
        {
            var ciudades = _ciudadServicio.GetAll(provinciaId);

            return Json(ciudades);

        }
    }
}
