﻿using Infraestructura;
using IServicio.Evento;
using MVC.Models.Evento;
using Rotativa;
using System.Linq;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class HomeController : Controller

    {
        private readonly IEventoServicio _eventoServicio;

        private EventManagerContext db = new EventManagerContext();

        public HomeController()
        {

        }

        public HomeController(IEventoServicio eventoServicio)
        {
            _eventoServicio = eventoServicio;
        }

        public ActionResult Index(string cadenaBuscar)
        {

            var evento = _eventoServicio.GetByFilter("");



            return View(evento.Select(x => new EventoVm()
            {
                Id = x.Id,
                Categoria_EventoId = x.Categoria_EventoId,
                CategoriaStr = x.CategoriaStr,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                Imagen = x.Imagen,
                FechaInicio = x.FechaInicio,
                FechaFin = x.FechaFin,
                HoraInicio = x.HoraInicio,
                HoraFin = x.HoraFin,
                Lugar = x.Lugar,
                Calle = x.Calle,
                Numero = x.Numero,
                Referencias = x.Referencias,
                OrganizadorId = x.OrganizadorId,
                OrganizadorStr = x.OrganizadorStr,
                OradorId = x.OradorId,
                OradorStr = x.OradorStr,
                ProvinciaId = x.ProvinciaId,
                ProvinciaStr = x.ProvinciaStr,
                CiudadId = x.CiudadId,
                CiudadStr = x.CiudadStr

            }).ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Reporte(string cadenaBuscar)
        {
            var evento = _eventoServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            return View(evento.Select(x => new EventoVm()
            {
                Id = x.Id,
                Categoria_EventoId = x.Categoria_EventoId,
                CategoriaStr = x.CategoriaStr,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                Imagen = x.Imagen,
                FechaInicio = x.FechaInicio,
                FechaFin = x.FechaFin,
                HoraInicio = x.HoraInicio,
                HoraFin = x.HoraFin,
                Lugar = x.Lugar,
                Calle = x.Calle,
                Numero = x.Numero,
                Referencias = x.Referencias,
                OrganizadorId = x.OrganizadorId,
                OrganizadorStr = x.OrganizadorStr,
                OradorId = x.OradorId,
                OradorStr = x.OradorStr,
                ProvinciaId = x.ProvinciaId,
                ProvinciaStr = x.ProvinciaStr,
                CiudadId = x.CiudadId,
                CiudadStr = x.CiudadStr

            }).ToList());
        }

        public ActionResult Imprimir()
        {
            return new ActionAsPdf("Reporte") { FileName = "Prueba.pdf" };
        }
    }
}