﻿using Infraestructura;
using IServicio.Ciudad;
using IServicio.Evento;
using IServicio.Orador;
using IServicio.Orador.Dto;
using MVC.Helpers.ComboBox.Ciudad;
using MVC.Helpers.ComboBox.Provincia;
using MVC.Helpers.ComboBox.Sexo;
using MVC.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class OradorController : Controller
    {
        private EventManagerContext db = new EventManagerContext();

        private readonly IOradorServicio _oradorServicio;
        private readonly IEventoServicio _eventoServicio;
        private readonly IComboBoxProvincia _comboBoxProvincia;
        private readonly IComboBoxCiudad _comboBoxCiudad;
        private readonly IComboBoxSexo _comboBoxSexo;
        private readonly ICiudadServicio _ciudadServicio;


        public OradorController()
        {

        }

        public OradorController(IOradorServicio oradorServicio, IEventoServicio eventoServicio
                                , IComboBoxProvincia comboBoxProvincia, IComboBoxCiudad comboBoxCiudad
                                , ICiudadServicio ciudadServicio, IComboBoxSexo comboBoxSexo)
        {
            _oradorServicio = oradorServicio;
            _eventoServicio = eventoServicio;
            _comboBoxProvincia = comboBoxProvincia;
            _comboBoxCiudad = comboBoxCiudad;
            _ciudadServicio = ciudadServicio;
            _comboBoxSexo = comboBoxSexo;
        }

        // GET: Oradors
        public ActionResult Index(string cadenaBuscar)
        {
            var oradores = _oradorServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            var lista = new List<OradorVm>();

            foreach (var item in oradores)
            {
                var eventos = _eventoServicio.GetAll().Where(x => x.OradorId == item.Id);

                if (eventos.Count() > 0)
                {
                    var orador = new OradorVm()
                    {
                        Id = item.Id,
                        Apellido = item.Apellido,
                        Nombre = item.Nombre,
                        Celular = item.Celular,
                        FechaNacimiento = item.FechaNacimiento,
                        Genero = item.Genero,
                        Cuil = item.Cuil,
                        Estado = item.Estado,
                        EstadoStr = item.Estado ? "Activo" : "Inactivo",
                        Telefono = item.Telefono,
                        ProvinciaStr = item.ProvinciaStr,
                        CiudadStr = item.CiudadStr,
                        EventosAsociados = true
                    };

                    lista.Add(orador);
                }

                else
                {
                    var orador = new OradorVm()
                    {
                        Id = item.Id,
                        Apellido = item.Apellido,
                        Nombre = item.Nombre,
                        Celular = item.Celular,
                        FechaNacimiento = item.FechaNacimiento,
                        Genero = item.Genero,
                        Cuil = item.Cuil,
                        Estado = item.Estado,
                        EstadoStr = item.Estado ? "Activo" : "Inactivo",
                        Telefono = item.Telefono,
                        ProvinciaStr = item.ProvinciaStr,
                        CiudadStr = item.CiudadStr,
                        EventosAsociados = false
                    };

                    lista.Add(orador);
                }
            }

           

            return View(lista.Select(x => new OradorVm()
            {
                Id = x.Id,
                Apellido = x.Apellido,
                Nombre = x.Nombre,
                Celular = x.Celular,
                FechaNacimiento = x.FechaNacimiento,
                Genero = x.Genero,
                Cuil = x.Cuil,
                Estado = x.Estado,
                EstadoStr = x.Estado ? "Activo" : "Inactivo",
                Telefono = x.Telefono,
                ProvinciaStr = x.ProvinciaStr,
                CiudadStr = x.CiudadStr,
                EventosAsociados = x.EventosAsociados

            }).ToList());
        }

        // GET: Oradors/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var orador = _oradorServicio.GetById(id);

            if (orador == null)
            {
                return HttpNotFound();
            }
            return View(new OradorVm()
            {
                Id = orador.Id,
                Apellido = orador.Apellido,
                Nombre = orador.Nombre,
                Celular = orador.Celular,
                FechaNacimiento = orador.FechaNacimiento,
                Genero = orador.Genero,
                Cuil = orador.Cuil,
                Estado = orador.Estado,
                Telefono = orador.Telefono,
                ProvinciaStr = orador.ProvinciaStr,
                CiudadStr = orador.CiudadStr
            });
        }

        // GET: Oradors/Create
        public ActionResult Create()
        {
            List<SelectListItem> listaCiudad = new List<SelectListItem>()
            {
            };

            return View(new OradorVm()
            {
                Provincias = _comboBoxProvincia.Poblar(),
                Ciudades = listaCiudad,
                Sexos = _comboBoxSexo.Poblar()
            });
        }

        // POST: Oradors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OradorVm oradorVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var oradorDto = CargarOradorDto(oradorVm);

                    _oradorServicio.Add(oradorDto);
                }
                return RedirectToAction("Index");

            }

            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                            ex.InnerException.InnerException != null &&
                            ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            List<SelectListItem> listaCiudad = new List<SelectListItem>()
            {
            };


            return View(new OradorVm
            {
                Sexos = _comboBoxSexo.Poblar(),
                Provincias = _comboBoxProvincia.Poblar(),
                Ciudades = listaCiudad
            });

        }


        // GET: Oradors/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var orador = _oradorServicio.GetById(id);

            if (orador == null)
            {
                return HttpNotFound();
            }
            return View(new OradorVm()
            {

                Apellido = orador.Apellido,
                Nombre = orador.Nombre,
                Celular = orador.Celular,
                FechaNacimiento = orador.FechaNacimiento,
                Genero = orador.Genero,
                Sexos = _comboBoxSexo.Poblar(),
                Cuil = orador.Cuil,
                Estado = orador.Estado,
                Telefono = orador.Telefono,
                ProvinciaId = orador.ProvinciaId,
                Provincias = _comboBoxProvincia.Poblar(),
                CiudadId = orador.CiudadId,
                Ciudades = _comboBoxCiudad.Poblar()

            });
        }

        // POST: Oradors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OradorVm oradorVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var orador = CargarOradorDto(oradorVm);

                    orador.Estado = oradorVm.Estado;

                    _oradorServicio.Update(orador);

                }

                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                            ex.InnerException.InnerException != null &&
                            ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    ModelState.AddModelError(string.Empty, "No se pudo crear el registro por que ya existe");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            return View(new OradorVm
            {
                Sexos = _comboBoxSexo.Poblar(),
                Provincias = _comboBoxProvincia.Poblar(),
                Ciudades = _comboBoxCiudad.Poblar()
            });

        }

        // GET: Oradors/Delete/5
        public ActionResult Delete(long? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var orador = _oradorServicio.GetById(id);

            if (orador == null)
            {
                return HttpNotFound();
            }
            return View(new OradorVm()
            {
                Apellido = orador.Apellido,
                Nombre = orador.Nombre,
                Celular = orador.Celular,
                FechaNacimiento = orador.FechaNacimiento,
                Genero = orador.Genero,
                Cuil = orador.Cuil,
                Estado = orador.Estado,
                Telefono = orador.Telefono,
                ProvinciaStr = orador.ProvinciaStr,
                CiudadStr = orador.CiudadStr
            });
        }

        // POST: Oradors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id, OradorVm oradorVm)
        {
            try
            {
                _oradorServicio.Delete(id);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "No se puede borrar, existen registros relacionados");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                return View(oradorVm);
            }
            
            return RedirectToAction("Index");
        }

        public ActionResult AsignarOrador(long id, string cadenaBuscar)
        {
            var eventoId = id;

            var orador = _oradorServicio.GetByFilter(!string.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar : string.Empty);

            ViewBag.EventoId = eventoId;


            return View(orador.Select(x => new OradorVm()
            {
                Id = x.Id,
                Apellido = x.Apellido,
                Nombre = x.Nombre,
                Celular = x.Celular,
                FechaNacimiento = x.FechaNacimiento,
                Genero = x.Genero,
                Cuil = x.Cuil,
                Estado = x.Estado,
                EstadoStr = x.Estado == true ? "Activo" : "Inactivo",
                Telefono = x.Telefono,
                ProvinciaStr = x.ProvinciaStr,
                CiudadStr = x.CiudadStr,
                EventoId = eventoId

            }).ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AsignarOrador(long oradorId, long eventoId, OradorVm oradorVm)
        {

            try
            {
                _eventoServicio.AsignarOrador(eventoId, oradorId);
            }
            catch (Exception ex)
            {

                return View(oradorVm);
            }


            return RedirectToAction("AdministrarEvento", "Evento", new { id = eventoId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //Metodos Privados

        private static OradorDto CargarOradorDto(OradorVm oradorVm)
        {
            return new OradorDto()
            {
                Id = oradorVm.Id,
                Apellido = oradorVm.Apellido,
                Nombre = oradorVm.Nombre,
                Celular = oradorVm.Celular,
                FechaNacimiento = oradorVm.FechaNacimiento,
                Genero = oradorVm.Genero == "1" ? "Masculino" : "Femenino",
                Cuil = oradorVm.Cuil,
                Telefono = oradorVm.Telefono,
                ProvinciaId = oradorVm.ProvinciaId,
                CiudadId = oradorVm.CiudadId
            };
        }

        public JsonResult ObtenerCiudades(long provinciaId)
        {
            var ciudades = _ciudadServicio.GetAll(provinciaId);

            return Json(ciudades);

        }
    }
}
