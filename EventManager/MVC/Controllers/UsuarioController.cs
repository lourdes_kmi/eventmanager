﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC.Models;
using MVC.Models.Seguridad;
using Rotativa;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class UsuarioController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Usuario
        public ActionResult Index()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var users = userManager.Users.ToList();
            var usersView = new List<UsuarioViewModel>();
            foreach (var user in users)
            {
                var userView = new UsuarioViewModel
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                    Email = user.Email
                };

                usersView.Add(userView);
            }

            return View(usersView);
        }

        public ActionResult Roles(string userId)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var roles = roleManager.Roles.ToList();
            var users = userManager.Users.ToList();
            var user = users.Find(x => x.Id == userId);

            var rolesView = new List<RoleView>();

            if (user.Roles != null)
            {

                foreach (var item in user.Roles)
                {
                    var role = roles.Find(x => x.Id == item.RoleId);

                    var roleView = new RoleView
                    {
                        RoleId = role.Id,
                        Nombre = role.Name
                    };

                    rolesView.Add(roleView);
                }
            }

            var userView = new UsuarioViewModel
            {
                UserId = user.Id,
                Email = user.Email,
                UserName = user.UserName,
                Roles = rolesView
            };

            return View(userView);
        }

        public ActionResult AddRole(string UserId)
        {
            if (string.IsNullOrEmpty(UserId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var usuarios = userManager.Users.ToList();
            var usuario = usuarios.Find(x => x.Id == UserId);

            if (usuario == null)
            {
                return HttpNotFound();
            }

            var usuarioView = new UsuarioViewModel()
            {
                UserId = usuario.Id,
                UserName = usuario.UserName,
                Email = usuario.Email
            };

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var list = roleManager.Roles.ToList();
            list.OrderBy(x => x.Name).ToList();
            list.Add(new IdentityRole
            {
                Id = "",
                Name = "-Seleccione un Rol-"
            });

            ViewBag.roleId = new SelectList(list, "Id", "Name");

            return View(usuarioView);
        }

        [HttpPost]
        public ActionResult AddRole(string UserId, FormCollection form)
        {
            var roleId = Request["roleId"]; //

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var usuarios = userManager.Users.ToList();
            var usuario = usuarios.Find(x => x.Id == UserId);
            var usuarioView = new UsuarioViewModel()
            {
                UserId = usuario.Id,
                UserName = usuario.UserName,
                Email = usuario.Email
            };


            if (string.IsNullOrEmpty(roleId))
            {
                ViewBag.Error = "Seleccione un rol";

                var list = roleManager.Roles.ToList();
                list.OrderBy(x => x.Name).ToList();
                list.Add(new IdentityRole
                {
                    Id = "",
                    Name = "-Seleccione un Rol-"
                });

                ViewBag.roleId = new SelectList(list, "Id", "Name");

                return View(usuarioView);



            }
            var roles = roleManager.Roles.ToList();
            var role = roles.Find(x => x.Id == roleId);

            if (!userManager.IsInRole(UserId, role.Name))
            {
                userManager.AddToRole(UserId, role.Name);
            }

            var rolesView = new List<RoleView>();

            foreach (var item in usuario.Roles)
            {
                role = roles.Find(x => x.Id == item.RoleId);

                var roleView = new RoleView
                {
                    RoleId = role.Id,
                    Nombre = role.Name
                };

                rolesView.Add(roleView);
            }


            usuarioView = new UsuarioViewModel
            {
                UserId = usuario.Id,
                Email = usuario.Email,
                UserName = usuario.UserName,
                Roles = rolesView
            };

            return View("Roles", usuarioView);
        }

        public ActionResult Delete(string userId, string roleId)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var usuarios = userManager.Users.ToList();
            var user = usuarios.Find(x => x.Id == userId);

            var usuarioView = new UsuarioViewModel()
            {
                UserId = user.Id,
                UserName = user.UserName,
                Email = user.Email
            };

            var roles = roleManager.Roles.ToList();
            var role = roles.Find(x => x.Id == roleId);

            if (userManager.IsInRole(userId, role.Name))
            {
                userManager.RemoveFromRole(userId, role.Name);
            }

            var rolesView = new List<RoleView>();

            foreach (var item in user.Roles)
            {
                role = roles.Find(x => x.Id == item.RoleId);

                var roleView = new RoleView
                {
                    RoleId = role.Id,
                    Nombre = role.Name
                };

                rolesView.Add(roleView);
            }


            usuarioView = new UsuarioViewModel
            {
                UserId = userId,
                Email = user.Email,
                UserName = user.UserName,
                Roles = rolesView
            };

            return View("Roles", usuarioView);


        }

        public ActionResult ReporteUsuario()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var users = userManager.Users.ToList();
            var usersView = new List<UsuarioViewModel>();
            foreach (var user in users)
            {
                var userView = new UsuarioViewModel
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                    Email = user.Email
                };

                usersView.Add(userView);
            }

            return View(usersView);
        }

        public ActionResult ImprimirUsuarios()
        {
            return new ActionAsPdf("Reporte") { FileName = "Prueba.pdf" };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}