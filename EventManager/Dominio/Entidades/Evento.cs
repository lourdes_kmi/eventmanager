﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Eventos")]
    [MetadataType(typeof(IEvento))]
    public class Evento : EntidadBase
    {
        //Propiedades
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Imagen { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime FechaFin { get; set; }

        public DateTime HoraInicio { get; set; }

        public DateTime HoraFin { get; set; }

        public string Lugar { get; set; }

        public string Calle { get; set; }

        public int Numero { get; set; }

        public string Referencias { get; set; }

        public string UsuarioNombre { get; set; }

        //propiedades de navegacion

        public long OrganizadorId { get; set; }

        public virtual Organizador Organizador { get; set; }

        public long Categoria_EventoId { get; set; }

        public virtual Categoria_Evento Categoria_Evento { get; set; }

        public long OradorId { get; set; }

        public virtual Oradors Orador { get; set; }

        public long ProvinciaId { get; set; }

        public virtual Provincia Provincia { get; set; }

        public long CiudadId { get; set; }

        public virtual Ciudad Ciudad { get; set; }

        public virtual ICollection<Oyente> Oyentes { get; set; }

        public virtual ICollection<Entrada> Entradas { get; set; }


    }
}
