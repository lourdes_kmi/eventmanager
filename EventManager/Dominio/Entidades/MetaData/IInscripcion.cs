﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades.MetaData
{
    public interface IInscripcion
    {
        DateTime FechaInscripcion { get; set; }

        string CodigoInscripcion { get; set; }
    }
}
