﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades.MetaData
{
    public interface IOrganizador
    {
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Range(1, 999)]
        [Index("Organizador_Legajo_Index", IsUnique = true)]
        int Legajo { get; set; }

        bool Estado { get; set; }
    }
}
