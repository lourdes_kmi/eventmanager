﻿namespace Dominio.Entidades.MetaData
{
    public interface ILineaResumen
    {
        string TipoEntrada { get; set; }

        decimal? Precio { get; set; }

        int Cantidad { get; set; }

        decimal? Subtotal { get; set; }
    }
}
