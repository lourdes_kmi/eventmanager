﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.Entidades.MetaData
{
    public interface IProvincia
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = " el campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Nombre { get; set; }
    }
}
