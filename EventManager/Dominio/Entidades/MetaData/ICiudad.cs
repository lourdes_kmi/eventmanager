﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.Entidades.MetaData
{
    public interface ICiudad
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DataType(DataType.PostalCode)]
        int CP { get; set; }
    }
}
