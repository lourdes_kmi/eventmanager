﻿namespace Dominio.Entidades.MetaData
{
    public interface IOyente
    {
        bool Estado { get; set; }

        string CodigoInscripcion { get; set; }

        int NumeroTransaccion { get; set; }
    }
}
