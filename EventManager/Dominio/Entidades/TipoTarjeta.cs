﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("TipoTarjeta")]
    [MetadataType(typeof(ITipoTarjeta))]
    public class TipoTarjeta : EntidadBase
    {
        public string Nombre { get; set; }
    }
}
