﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Entradas")]
    [MetadataType(typeof(IEntrada))]
    public class Entrada : EntidadBase
    {
        //Propiedades

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public int Cantidad { get; set; }

        public decimal? Precio { get; set; }

        public string TipoEntrada { get; set; }

        public DateTime FechaInicioVenta { get; set; }

        public DateTime FechaFinVenta { get; set; }

        public int CantidadMaximaPorCompra { get; set; }

        public string CanalDeVentas { get; set; }

        public int CantidadVendida { get; set; }

        //Propiedades de Navegacion

        public long EventoId { get; set; }

        public virtual Evento Evento { get; set; }

    }
}
