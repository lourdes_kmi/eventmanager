﻿using Dominio.Entidades.MetaData;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Persona_Orador")]
    [MetadataType(typeof(IOrador))]
    public class Oradors : Persona
    {
        //Propiedades

        //agregar legajo

        public bool Estado { get; set; }

        //Propiedades de Navegacion

        public virtual IEnumerable<Evento> Eventos { get; set; }

    }
}
