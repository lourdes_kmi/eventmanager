﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{

    [Table("ListaBancos")]
    [MetadataType(typeof(IListaBancos))]
    public class ListaBancos: EntidadBase
    {
        public string Nombre { get; set; }
    }
}
