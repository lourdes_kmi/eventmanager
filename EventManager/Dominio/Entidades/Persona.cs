﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Personas")]
    [MetadataType(typeof(IPersona))]
    public class Persona : EntidadBase
    {
        //Propiedades

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Cuil { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public string Genero { get; set; }

        public string Telefono { get; set; }

        public string Celular { get; set; }

        //Propiedades de Navegacion

        public long ProvinciaId { get; set; }

        public virtual Provincia Provincia { get; set; }

        public long CiudadId { get; set; }

        public virtual Ciudad Ciudad { get; set; }

    }
}
