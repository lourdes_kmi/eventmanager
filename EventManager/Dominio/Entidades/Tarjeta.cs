﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Tarjetas")]
    [MetadataType(typeof(ITarjeta))]
    public class Tarjeta : EntidadBase
    {
        //Propiedades

        public string TipoTarjeta { get; set; } //Enum

        public string NombreTitular { get; set; }

        public string Numero { get; set; }

        public int DniTitular { get; set; }

        public int CodigoSeguridad { get; set; }

        public string FechaExpiracion { get; set; }

        public string BancoEmisor { get; set; }

        public int Cuotas { get; set; } //Enum

    }
}
