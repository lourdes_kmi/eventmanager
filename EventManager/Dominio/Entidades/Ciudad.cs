﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Ciudades")]
    [MetadataType(typeof(ICiudad))]
    public class Ciudad : EntidadBase
    {
        //Propiedades
        public string Nombre { get; set; }

        public int CP { get; set; }


        //Propiedades de Navegacion

        public long ProvinciaId { get; set; }

        public virtual Provincia Provincia { get; set; }

        public virtual IEnumerable<Evento> Eventos { get; set; }
    }
}
