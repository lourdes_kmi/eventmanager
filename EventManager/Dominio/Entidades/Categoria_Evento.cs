﻿using Dominio.Entidades.MetaData;
using DominioBase;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Categoria_Eventos")]
    [MetadataType(typeof(ICategoria))]
    public class Categoria_Evento : EntidadBase
    {
        //Propiedades

        public string Descripcion { get; set; }

        public bool Estado { get; set; }

        //Propiedades de Navegacion

        public virtual IEnumerable<Evento> Eventos { get; set; }

    }
}
