﻿using Repositorio.Base;

namespace Dominio.Repositorio.Persona
{
    public interface IPersonaRepositorio : IRepositorio<Entidades.Persona>
    {

    }
}
