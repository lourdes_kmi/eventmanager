﻿using Repositorio.Base;

namespace Dominio.Repositorio.Provincia
{
    public interface IProvinciaRepositorio : IRepositorio<Entidades.Provincia>
    {

    }
}
