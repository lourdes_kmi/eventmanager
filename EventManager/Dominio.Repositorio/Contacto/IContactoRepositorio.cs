﻿using Repositorio.Base;

namespace Dominio.Repositorio.Contacto
{
    public interface IContactoRepositorio : IRepositorio<Entidades.Contacto>
    {

    }
}
