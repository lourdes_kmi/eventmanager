﻿using Repositorio.Base;

namespace Dominio.Repositorio.Tarjeta
{
    public interface ITarjetaRepositorio : IRepositorio<Entidades.Tarjeta>
    {

    }
}
