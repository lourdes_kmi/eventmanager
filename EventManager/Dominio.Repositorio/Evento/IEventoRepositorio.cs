﻿using Repositorio.Base;

namespace Dominio.Repositorio.Evento
{
    public interface IEventoRepositorio : IRepositorio<Entidades.Evento>
    {

    }
}
