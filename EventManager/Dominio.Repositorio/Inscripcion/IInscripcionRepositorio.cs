﻿using Repositorio.Base;

namespace Dominio.Repositorio.Inscripcion
{
    public interface IInscripcionRepositorio : IRepositorio<Entidades.Inscripcion>
    {

    }
}
