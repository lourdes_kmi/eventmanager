﻿using System.Collections.Generic;

namespace IServicio.Sexo
{
    public interface ISexoServicio
    {
        IEnumerable<SexoDto> ObtenerSexo();

    }
}
