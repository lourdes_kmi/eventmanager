﻿using IServicio.Provincia.Dto;
using System.Collections.Generic;

namespace IServicio.Provincia
{
    public interface IProvinciaServicio
    {
        void Add(ProvinciaDto Dto);
        void Update(ProvinciaDto Dto);
        void Delete(long Id);

        IEnumerable<ProvinciaDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        ProvinciaDto GetById(long? Id);
    }
}
