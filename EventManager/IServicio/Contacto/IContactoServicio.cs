﻿using IServicio.Contacto.Dto;
using System.Collections.Generic;

namespace IServicio.Contacto
{
    public interface IContactoServicio
    {
        void Add(ContactoDto Dto);
        void Update(ContactoDto Dto);
        void Delete(long Id);

        IEnumerable<ContactoDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        ContactoDto GetById(long? Id);
    }
}
