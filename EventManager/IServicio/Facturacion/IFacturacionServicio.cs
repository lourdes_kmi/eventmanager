﻿using IServicio.Faturacion.Dto;
using System.Collections.Generic;

namespace IServicio.Facturacion
{
    public interface IFacturacionServicio
    {
        void Add(FacturacionDto Dto);
        void Update(FacturacionDto Dto);
        void Delete(long Id);

        IEnumerable<FacturacionDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        FacturacionDto GetById(long? Id);
    }
}
