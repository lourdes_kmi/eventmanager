﻿using Servicio.Base.Dtos;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IServicio.Faturacion.Dto
{
    public class FacturacionDto : DtoBase
    {

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Codigo Postal")]
        public int CodPostal { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Tarjeta")]
        public long TarjetaId { get; set; }

        [Display(Name = "Tarjeta")]
        public string TarjetaStr { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Provincia")]
        public long ProvinciaId { get; set; }

        [Display(Name = "Provincia")]
        public string ProvinciaStr { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Ciudad")]
        public long CiudadId { get; set; }

        [Display(Name = "Ciudad")]
        public string CiudadStr { get; set; }

    }
}
