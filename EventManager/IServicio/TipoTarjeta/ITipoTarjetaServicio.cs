﻿using IServicio.TipoTarjeta.Dto;
using System.Collections.Generic;

namespace IServicio.TipoTarjeta
{
    public interface ITipoTarjetaServicio
    {
        IEnumerable<TipoTarjetaDto> GetAll();
    }
}
