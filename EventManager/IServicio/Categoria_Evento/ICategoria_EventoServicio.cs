﻿using IServicio.Categoria_Evento.Dto;
using System.Collections.Generic;

namespace IServicio.Categoria_Evento
{
    public interface ICategoria_EventoServicio
    {
        void Add(Categoria_EventoDto Dto);
        void Update(Categoria_EventoDto Dto);
        void Delete(long Id);

        IEnumerable<Categoria_EventoDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        Categoria_EventoDto GetById(long? Id);


    }
}
