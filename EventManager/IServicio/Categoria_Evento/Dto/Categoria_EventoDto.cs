﻿using Servicio.Base.Dtos;

namespace IServicio.Categoria_Evento.Dto
{
    public class Categoria_EventoDto : DtoBase
    {
        public string Descripcion { get; set; }

        public bool Estado { get; set; }
    }
}
