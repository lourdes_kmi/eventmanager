﻿using Servicio.Base.Dtos;
using System;
using System.ComponentModel.DataAnnotations;

namespace IServicio.Tarjeta.Dto
{
    public class TarjetaDto : DtoBase
    {

        //Propiedades

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Tipo de Tarjeta")]
        public string TipoTarjeta { get; set; } //Enum

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [Display(Name = "Nombre del Titular")]
        public string NombreTitular { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        //[CreditCard]
        public string Numero { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Dni")]
        public int DniTitular { get; set; }

        [Display(Name = "Codigo de Seguridad")]
        public int CodigoSeguridad { get; set; }

        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //[Display(Name = "Fecha de Expiracion")]
        //public DateTime FechaExpiracion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(5, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [Display(Name = "Fecha de Expiracion")]
        public string FechaExpiracion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [Display(Name = "Banco Emisor")]
        public string BancoEmisor { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public int Cuotas { get; set; } //Enum

        public string TarjetaStr { get { return TipoTarjeta + " " + BancoEmisor + " " + NombreTitular; } }
    }
}
