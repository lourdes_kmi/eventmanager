﻿using IServicio.Tarjeta.Dto;
using System.Collections.Generic;

namespace IServicio.Tarjeta
{
    public interface ITarjetaServicio
    {
        void Add(TarjetaDto Dto);
        void Update(TarjetaDto Dto);
        void Delete(long Id);

        IEnumerable<TarjetaDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        TarjetaDto GetById(long? Id);

        long ObtenerSiguienteId();
    }
}
