﻿using Servicio.Base.Dtos;
using System;
using System.ComponentModel.DataAnnotations;

namespace IServicio.Entrada.Dto
{
    public class EntradaDto : DtoBase
    {
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public int Cantidad { get; set; }

        public decimal? Precio { get; set; }

        [Display(Name = "Tipo de Entrada")]
        public string TipoEntrada { get; set; }

        public DateTime FechaInicioVenta { get; set; }

        public DateTime FechaFinVenta { get; set; }

        public int CantidadMaximaPorCompra { get; set; }

        public int CantidadVendida { get; set; }

        public string CanalDeVentas { get; set; }

        public long EventoId { get; set; }

        public string EventoStr { get; set; }

        public int CantSeleccionada { get; set; }
    }
}
