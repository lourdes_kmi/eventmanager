﻿using IServicio.Entrada.Dto;
using IServicio.Evento.Dto;
using Servicio.Base.Dtos;
using System;
using System.ComponentModel.DataAnnotations;

namespace IServicio.Oyente.Dto
{
    public class OyenteDto : DtoBase
    {
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Nombre { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Apellido { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(10, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Celular { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio.")]
        [StringLength(50, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        public string Direccion { get; set; }

        public bool Estado { get; set; }

        public string CodigoInscripcion { get; set; }

        public int NumeroTransaccion { get; set; }

        [Display(Name = "Evento")]
        public long EventoId { get; set; }

        [Display(Name = "Evento")]
        public string EventoStr { get; set; }

        [Display(Name = "Entrada")]
        public long EntradaId { get; set; }

        [Display(Name = "Entrada")]
        public string EntradaStr { get; set; }

        public EventoDto Evento { get; set; }

        public EntradaDto Entrada { get; set; }
    }
}
