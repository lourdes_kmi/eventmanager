﻿using IServicio.Evento.Dto;
using System.Collections.Generic;

namespace IServicio.Evento
{
    public interface IEventoServicio
    {
        void Add(EventoDto Dto);
        void Update(EventoDto Dto);
        void Delete(long Id);

        IEnumerable<EventoDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        IEnumerable<EventoDto> GetAll();

        IEnumerable<EventoDto> GetByUserName(string usuarioNombre); // obtener por UsuarioId

        EventoDto GetById(long? Id);

        void AsignarOrador(long? eventoId, long oradorId);

        void AsignarOrganizador(long? eventoId, long organizadorId);
    }
}
