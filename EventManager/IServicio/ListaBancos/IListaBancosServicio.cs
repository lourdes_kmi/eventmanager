﻿using IServicio.ListaBancos.Dto;
using System.Collections.Generic;

namespace IServicio.ListaBancos
{
    public interface IListaBancosServicio
    {
        IEnumerable<ListaBancosDto> GetAll(); 
    }
}
