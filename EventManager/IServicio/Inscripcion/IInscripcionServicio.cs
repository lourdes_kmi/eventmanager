﻿using IServicio.Inscripcion.Dto;
using System.Collections.Generic;
using System.Web;

namespace IServicio.Inscripcion
{
    public interface IInscripcionServicio
    {
        void Add(InscripcionDto Dto);
        void Update(InscripcionDto Dto);
        void Delete(long Id);

        IEnumerable<InscripcionDto> GetByFilter(string cadenaBuscar); // obtener por filtro

        InscripcionDto GetById(long? Id);

        IEnumerable<InscripcionDto> GetAll();

        long ObtenerSiguienteId();

    }
}
