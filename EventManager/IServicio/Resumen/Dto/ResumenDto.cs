﻿using IServicio.LineaResumen.Dto;
using Servicio.Base.Dtos;
using System.Collections.Generic;

namespace IServicio.Resumen.Dto
{
    public class ResumenDto : DtoBase
    {
        public decimal? Total { get; set; }

        public List<LineaResumenDto> LineaResumenes { get; set; }
    }
}
