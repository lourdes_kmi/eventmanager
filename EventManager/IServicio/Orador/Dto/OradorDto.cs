﻿using Servicio.Base.Dtos;
using System;

namespace IServicio.Orador.Dto
{
    public class OradorDto : DtoBase
    {
        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string ApyNom { get { return Nombre + " " + Apellido; } }

        public string Cuil { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public string Genero { get; set; }

        public string Telefono { get; set; }

        public string Celular { get; set; }

        public bool Estado { get; set; }

        public long ProvinciaId { get; set; }

        public string ProvinciaStr { get; set; }

        public long CiudadId { get; set; }

        public string CiudadStr { get; set; }

        public long? EventoId { get; set; } //se usa en evento asignarorador
    }
}
